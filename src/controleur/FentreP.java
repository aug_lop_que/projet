/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controleur;

import modele.BarChart2;
import modele.PieChart2;
import java.sql.SQLException;
import jdbcv2018.*;
import modele.PieChart;
import modele.BarChart;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.JCheckBox;
import javax.swing.JFormattedTextField;
import static javax.swing.SwingConstants.TOP;
import javax.swing.JPanel;
import java.awt.GridLayout;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.general.PieDataset;
import org.jfree.ui.ApplicationFrame;
import org.jfree.ui.RefineryUtilities;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
 
/**
 *
 * @author adrie
 */
public class FentreP extends JFrame implements ActionListener{
    ///Attributs
    private JPanel panelP = new JPanel(); /// Panel principal
    private JTabbedPane onglets = new JTabbedPane(TOP); ///Onglet
    
    ///Panels de differents onglets
    ///Reporting
    JPanel reporting = new JPanel(new GridLayout(2,2)); ///1er onglet reporting

    PieChart Pie = new PieChart();
    BarChart Bar = new BarChart("Nb Malades par mutuelle");
    BarChart2 Bar2 = new BarChart2("Moyenne des salaires par service");
    PieChart2 Pie2 = new PieChart2();
    ///Requête
    private JPanel requete = new JPanel(); ///2e onglet Requête
    private JComboBox choixTable = new JComboBox(); ///Menu déroulant 1er onglet
    private JComboBox choixChamp[] = new JComboBox[8]; //Tableau de menu déroulants pour le choix du champ à récupérer
    private JComboBox choixChamp2[] = new JComboBox[8]; //Tableau de menu déroulants pour le choix du champ à récupérer
    private JComboBox choixChamp3[] = new JComboBox[8]; //Tableau de menu déroulants pour le choix du champ à récupérer
    private JComboBox choixChamp4[] = new JComboBox[8]; //Tableau de menu déroulants pour le choix du champ à récupérer
    private JButton valider2 = new JButton("Rechercher"); /// Bouton de validation
    private JTextField text1 = new JTextField(); //Champ de texte
    private JTextField text2 = new JTextField(); //Champ de texte
    private JTextField text3 = new JTextField(); //Champ de texte
    private JLabel Affi1 = new JLabel(); //Label d'affichage
    private JLabel Affi2 = new JLabel(); //Label d'affichage
    private JLabel Affi3 = new JLabel(); //Label d'affichage
    private JLabel Affi4 = new JLabel(); //Label d'affichage
    private JLabel Affi5 = new JLabel(); //Label d'affichage
    private JComboBox choixCondition = new JComboBox();
    private JComboBox nombreCondition = new JComboBox();
    private JLabel AffichageFenetre = new JLabel();
    private String tableauObj[][] = new String[100][100];
    private String col[];
    private String col1,col2,col3,col4,col5,col6;
    private Object[] data = new Object[100];
    private JTable ResultatRecherche;
    
    
    
    
   ///Modification
    private JPanel modif = new JPanel(); ///3e onglet Modification
    private JComboBox choixModif = new JComboBox(); ///Menu déroulant du type de modifications à apporter
    private JComboBox choixTab = new JComboBox(); ///Menu déroulant pour le choix de la table à modifier
    private JButton valider = new JButton("Valider"); /// Bouton de validation
    ///Ensemble des texte des tables
    ///Table service
    private JLabel labelCodeService = new JLabel("Code du service"); ///titre du texte
    private JTextField codeService = new JTextField("string");
    private JComboBox codeListeService = new JComboBox(); ///liste des différents clé primaire existante pour la table service
    private JLabel labelNomService = new JLabel("Nom du service"); ///titre du texte
    private JTextField nomService = new JTextField("string");
    private JLabel labelBatimentService = new JLabel("Lettre du batiment"); ///titre du texte
    private JTextField batimentService = new JTextField("string");
    private JLabel labelDirecteurService = new JLabel("Numero directeur du service"); ///titre du texte
    private JFormattedTextField directeurService = new JFormattedTextField(NumberFormat.getIntegerInstance());
    ///Table chambre
    private JLabel labelCode_serviceChambre = new JLabel("Code de la chambre"); ///titre du texte
    private JTextField code_serviceChambre = new JTextField("string");
    private JComboBox code_serviceListeChambre = new JComboBox(); ///liste des différents clé secondaire existante pour la table chambre
    private JLabel labelNo_chambreChambre = new JLabel("Numero de la chambre"); ///titre du texte
    private JFormattedTextField no_chambreChambre = new JFormattedTextField(NumberFormat.getIntegerInstance());
    private JComboBox no_chambreListeChambre = new JComboBox(); ///liste des différents clé primaire existante pour la table chambre
    private JLabel labelSurveillantChambre = new JLabel("Numero du surveillant"); ///titre du texte
    private JFormattedTextField surveillantChambre = new JFormattedTextField(NumberFormat.getIntegerInstance());
    private JLabel labelNb_litsChambre = new JLabel("Nombre de lits"); ///titre du texte
    private JFormattedTextField nb_litsChambre = new JFormattedTextField(NumberFormat.getIntegerInstance());
    ///table employee
    private JLabel labelNumeroEmployee = new JLabel("Numero de l'employee"); ///titre du texte
    private JFormattedTextField numeroEmployee = new JFormattedTextField(NumberFormat.getIntegerInstance());
    private JComboBox numeroListeEmployee = new JComboBox(); ///liste des différents clé primaire existante pour la table chambre
    private JLabel labelNomEmployee = new JLabel("Nom de l'employee"); ///titre du texte
    private JTextField nomEmployee = new JTextField("string");
    private JLabel labelPrenomEmployee = new JLabel("Prenom de l'employee"); ///titre du texte
    private JTextField prenomEmployee = new JTextField("string");
    private JLabel labelAdresseEmployee = new JLabel("Adresse de l'employee"); ///titre du texte
    private JTextField adresseEmployee = new JTextField("string");
    private JLabel labelTelEmployee = new JLabel("Numero de telephone de l'employee"); ///titre du texte
    private JTextField telEmployee = new JTextField("string");
    ///table docteur
    private JLabel labelNumeroDocteur = new JLabel("Numero du docteur"); ///titre du texte
    private JFormattedTextField numeroDocteur = new JFormattedTextField(NumberFormat.getIntegerInstance());
    private JComboBox numeroListeDocteur = new JComboBox(); ///liste des différents clé primaire existante pour la table chambre
    private JLabel labelSpecialiteDocteur = new JLabel("Speciaite du docteur"); ///titre du texte
    private JTextField specialiteDocteur = new JTextField("string");
    ///table infirmier
    private JLabel labelNumeroInfirmier = new JLabel("Numero de l'infirmier"); ///titre du texte
    private JFormattedTextField numeroInfirmier = new JFormattedTextField(NumberFormat.getIntegerInstance());
    private JComboBox numeroListeInfirmier = new JComboBox(); ///liste des différents clé primaire existante pour la table chambre
    private JLabel labelCode_serviceInfirmier = new JLabel("Code de service de l'infirmier"); ///titre du texte
    private JTextField code_serviceInfirmier = new JTextField("string");
    private JLabel labelRotationInfirmier = new JLabel("Rotation de l'infirmier"); ///titre du texte
    private JComboBox rotationInfirmier = new JComboBox();
    private JLabel labelSalaireInfirmier = new JLabel("Salaire de l'infirmier"); ///titre du texte
    private JFormattedTextField salaireInfirmier = new JFormattedTextField(NumberFormat.getIntegerInstance());
    ///Malade
    private JLabel labelNumeroMalade = new JLabel("Numero du malade"); ///titre du texte
    private JFormattedTextField numeroMalade = new JFormattedTextField(NumberFormat.getIntegerInstance());
    private JComboBox numeroListeMalade = new JComboBox();
    private JLabel labelNomMalade = new JLabel("Nom du malade"); ///titre du texte
    private JTextField nomMalade = new JTextField("string");
    private JLabel labelPrenomMalade = new JLabel("Nom du malade"); ///titre du texte
    private JTextField prenomMalade = new JTextField("string");
    private JLabel labelAdresseMalade = new JLabel("Nom du malade"); ///titre du texte
    private JTextField adresseMalade = new JTextField("string");
    private JLabel labelTelMalade = new JLabel("Numero de telephone du malade"); ///titre du texte
    private JFormattedTextField telMalade = new JFormattedTextField(NumberFormat.getIntegerInstance());
    private JLabel labelMutuelleMalade = new JLabel("Assurance du malade"); ///titre du texte
    private JTextField mutuelleMalade = new JTextField("string");
    ///Hospitalisation
    private JLabel labelNo_maladeHospitalisation = new JLabel("Numero du malade hospitalisé"); ///titre du texte
    private JFormattedTextField no_maladeHospitalisation = new JFormattedTextField(NumberFormat.getIntegerInstance());
    private JComboBox no_maladeListeHospitalisation = new JComboBox();
    private JLabel labelCode_serviceHospitalisation = new JLabel("Code de service de l'hospitalisation"); ///titre du texte
    private JTextField code_serviceHospitalisation = new JTextField("string");
    private JLabel labelNo_chambreHospitalisation = new JLabel("Numero de chambre d'hospitalisation"); ///titre du texte
    private JFormattedTextField no_chambreHospitalisation = new JFormattedTextField(NumberFormat.getIntegerInstance());
    private JLabel labelLitHospitalisation = new JLabel("Nombre de lits dans la chambre"); ///titre du texte
    private JFormattedTextField litHospitalisation = new JFormattedTextField(NumberFormat.getIntegerInstance());
    ///Soignee
    private JLabel labelNo_docteurSoignee = new JLabel("Numero de chambre d'hospitalisation"); ///titre du texte
    private JFormattedTextField no_docteurSoignee = new JFormattedTextField(NumberFormat.getIntegerInstance());
    private JComboBox no_docteurListeSoignee = new JComboBox();
    private JLabel labelNo_maladeSoignee = new JLabel("Nombre de lits dans la chambre"); ///titre du texte
    private JFormattedTextField no_maladeSoignee = new JFormattedTextField(NumberFormat.getIntegerInstance());
    
    public String requeteF; ///Champ de requete
    
    public FentreP() throws Exception
    {
        ///Instanciation de la fênetre**********************************************************************************
        super();
        this.setTitle("Logiciel de gestion d'un hôpital");
        this.setSize(1280, 900); /// Definit la taille de la fênetre
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
      
        
        ///Creation de l'onglet Reporting********************************************************************************
        JFreeChart chart = Pie.createPieChart(); // creation du chart  1 
        JFreeChart chart2 = Bar.createBarChart();// creation du chart  2
        JFreeChart chart3 = Bar2.createBarChart();// creation du chart  3 
        JFreeChart chart4 = Pie2.createPieChart();// creation du chart  4 
        
        ChartPanel chartpanel = new ChartPanel(chart); //transformation en chartpanel pour mettre dans jpanel
        ChartPanel chartpanel2 = new ChartPanel(chart2);//transformation en chartpanel pour mettre dans jpanel
        ChartPanel chartpanel3 = new ChartPanel(chart3);//transformation en chartpanel pour mettre dans jpanel
        ChartPanel chartpanel4 = new ChartPanel(chart4);//transformation en chartpanel pour mettre dans jpanel
        
        reporting.setPreferredSize(new Dimension(1000, 780));//dimension onglet reporting
        reporting.setBackground(Color.white); ///Couleur de fond de l'onglet 1
        onglets.addTab("Reporting", reporting);

        //ImageIcon image = new ImageIcon("PieChart.jpeg");
        //JLabel titreOnglet1 = new JLabel("", image ,JLabel.CENTER); ///Instancition de l'onglet Reporting        
        //reporting.add(titreOnglet1, BorderLayout.NORTH); //si on utilise une image (idee moins bonne mais garder au cas ou)
        
        chartpanel.setDomainZoomable(true); //ajout du chartpanel
        chartpanel.setPreferredSize(new Dimension(320,160));
        reporting.add(chartpanel);
        
        chartpanel2.setDomainZoomable(true); //ajout du chartpanel
        chartpanel2.setPreferredSize(new Dimension(320,160));
        reporting.add(chartpanel2); 
        
        chartpanel3.setDomainZoomable(true); //ajout du chartpanel
        chartpanel3.setPreferredSize(new Dimension(320,160));
        reporting.add(chartpanel3); 

        chartpanel4.setDomainZoomable(true); //ajout du chartpanel
        chartpanel4.setPreferredSize(new Dimension(320,160));
        reporting.add(chartpanel4);
        
        
        
        ///Creation de l'onglet Requête***********************************************************************************
        requete.setPreferredSize(new Dimension(1000, 780));
        requete.setBackground(Color.white); ///Couleur de fond de l'onglet 2
        onglets.addTab("Requête", requete);
        //Création des menus déroulants
        choixTable.setPreferredSize(new Dimension(150, 20)); ///Definition du menu déroulant pour choisir l'option de modification
        ///Ajout des différentes options
        choixTable.addItem("Service");
        choixTable.addItem("Chambre");
        choixTable.addItem("Employe");
        choixTable.addItem("Docteur");
        choixTable.addItem("Infirmier");
        choixTable.addItem("Malade");
        choixTable.addItem("Hospitalisation");
        choixTable.addItem("Soigne");
        
        nombreCondition.addItem("1");
        nombreCondition.addItem("2");
        nombreCondition.addItem("3");
        
       
        //choixChamp[0].setPreferredSize(new Dimension(150, 20)); ///Definition du menu déroulant pour choisir l'option de modification
        
        //Création des choix possibles pour les combo box de sélection de champ
        choixChamp[0] = new JComboBox();
        choixChamp[0].addItem("*");
        choixChamp[0].addItem("Code");
        choixChamp[0].addItem("Nom");
        choixChamp[0].addItem("Batiment");
        choixChamp[0].addItem("Directeur");
        
        choixChamp[1] = new JComboBox();
        choixChamp[1].addItem("*");
        choixChamp[1].addItem("Code_service");
        choixChamp[1].addItem("No_chambre");
        choixChamp[1].addItem("Surveillant");
        choixChamp[1].addItem("Nb_lits");
        
        choixChamp[2] = new JComboBox();
        choixChamp[2].addItem("*");
        choixChamp[2].addItem("Numero");
        choixChamp[2].addItem("Nom");
        choixChamp[2].addItem("Prenom");
        choixChamp[2].addItem("Adresse");
        choixChamp[2].addItem("Tel");
        
        choixChamp[3] = new JComboBox();
        choixChamp[3].addItem("*");
        choixChamp[3].addItem("Numero");
        choixChamp[3].addItem("Specialite");
        
        choixChamp[4] = new JComboBox();
        choixChamp[4].addItem("*");
        choixChamp[4].addItem("Numero");
        choixChamp[4].addItem("Code_service");
        choixChamp[4].addItem("Rotation");
        choixChamp[4].addItem("Salaire");
        
        choixChamp[5] = new JComboBox();
        choixChamp[5].addItem("*");
        choixChamp[5].addItem("Numero");
        choixChamp[5].addItem("Nom");
        choixChamp[5].addItem("Prenom");
        choixChamp[5].addItem("Adresse");
        choixChamp[5].addItem("Tel");
        choixChamp[5].addItem("Mutuelle");
        
        choixChamp[6] = new JComboBox();
        choixChamp[6].addItem("*");
        choixChamp[6].addItem("No_malade");
        choixChamp[6].addItem("Code_service");
        choixChamp[6].addItem("No_chambre");
        choixChamp[6].addItem("Lit");
        
        choixChamp[7] = new JComboBox();
        choixChamp[7].addItem("*");
        choixChamp[7].addItem("No_Docteur");
        choixChamp[7].addItem("No_Malade");
        
        //choixChamp[0].setPreferredSize(new Dimension(150, 20)); ///Definition du menu déroulant pour choisir l'option de modification
        
        //Création des choix possibles pour les combo box de sélection de champ
        choixChamp2[0] = new JComboBox();
        choixChamp2[0].addItem("Code");
        choixChamp2[0].addItem("Nom");
        choixChamp2[0].addItem("Batiment");
        choixChamp2[0].addItem("Directeur");
        
        choixChamp2[1] = new JComboBox();
        choixChamp2[1].addItem("Code_service");
        choixChamp2[1].addItem("No_chambre");
        choixChamp2[1].addItem("Surveillant");
        choixChamp2[1].addItem("Nb_lits");
        
        choixChamp2[2] = new JComboBox();
        choixChamp2[2].addItem("Numero");
        choixChamp2[2].addItem("Nom");
        choixChamp2[2].addItem("Prenom");
        choixChamp2[2].addItem("Adresse");
        choixChamp2[2].addItem("Tel");
        
        choixChamp2[3] = new JComboBox();
        choixChamp2[3].addItem("Numero");
        choixChamp2[3].addItem("Specialite");
        
        choixChamp2[4] = new JComboBox();
        choixChamp2[4].addItem("Numero");
        choixChamp2[4].addItem("Code_service");
        choixChamp2[4].addItem("Rotation");
        choixChamp2[4].addItem("Salaire");
        
        choixChamp2[5] = new JComboBox();
        choixChamp2[5].addItem("Numero");
        choixChamp2[5].addItem("Nom");
        choixChamp2[5].addItem("Prenom");
        choixChamp2[5].addItem("Adresse");
        choixChamp2[5].addItem("Tel");
        choixChamp2[5].addItem("Mutuelle");
        
        choixChamp2[6] = new JComboBox();
        choixChamp2[6].addItem("No_malade");
        choixChamp2[6].addItem("Code_service");
        choixChamp2[6].addItem("No_chambre");
        choixChamp2[6].addItem("Lit");
        
        choixChamp2[7] = new JComboBox();
        choixChamp2[7].addItem("No_Docteur");
        choixChamp2[7].addItem("No_Malade");
        
        choixChamp3[0] = new JComboBox();
        choixChamp3[0].addItem("Code");
        choixChamp3[0].addItem("Nom");
        choixChamp3[0].addItem("Batiment");
        choixChamp3[0].addItem("Directeur");
        
        choixChamp3[1] = new JComboBox();
        choixChamp3[1].addItem("Code_service");
        choixChamp3[1].addItem("No_chambre");
        choixChamp3[1].addItem("Surveillant");
        choixChamp3[1].addItem("Nb_lits");
        
        choixChamp3[2] = new JComboBox();
        choixChamp3[2].addItem("Numero");
        choixChamp3[2].addItem("Nom");
        choixChamp3[2].addItem("Prenom");
        choixChamp3[2].addItem("Adresse");
        choixChamp3[2].addItem("Tel");
        
        choixChamp3[3] = new JComboBox();
        choixChamp3[3].addItem("Numero");
        choixChamp3[3].addItem("Specialite");
        
        choixChamp3[4] = new JComboBox();
        choixChamp3[4].addItem("Numero");
        choixChamp3[4].addItem("Code_service");
        choixChamp3[4].addItem("Rotation");
        choixChamp3[4].addItem("Salaire");
        
        choixChamp3[5] = new JComboBox();
        choixChamp3[5].addItem("Numero");
        choixChamp3[5].addItem("Nom");
        choixChamp3[5].addItem("Prenom");
        choixChamp3[5].addItem("Adresse");
        choixChamp3[5].addItem("Tel");
        choixChamp3[5].addItem("Mutuelle");
        
        choixChamp3[6] = new JComboBox();
        choixChamp3[6].addItem("No_malade");
        choixChamp3[6].addItem("Code_service");
        choixChamp3[6].addItem("No_chambre");
        choixChamp3[6].addItem("Lit");
        
        choixChamp3[7] = new JComboBox();
        choixChamp3[7].addItem("No_Docteur");
        choixChamp3[7].addItem("No_Malade");
        
        choixChamp4[0] = new JComboBox();
        choixChamp4[0].addItem("Code");
        choixChamp4[0].addItem("Nom");
        choixChamp4[0].addItem("Batiment");
        choixChamp4[0].addItem("Directeur");
        
        choixChamp4[1] = new JComboBox();
        choixChamp4[1].addItem("Code_service");
        choixChamp4[1].addItem("No_chambre");
        choixChamp4[1].addItem("Surveillant");
        choixChamp4[1].addItem("Nb_lits");
        
        choixChamp4[2] = new JComboBox();
        choixChamp4[2].addItem("Numero");
        choixChamp4[2].addItem("Nom");
        choixChamp4[2].addItem("Prenom");
        choixChamp4[2].addItem("Adresse");
        choixChamp4[2].addItem("Tel");
        
        choixChamp4[3] = new JComboBox();
        choixChamp4[3].addItem("Numero");
        choixChamp4[3].addItem("Specialite");
        
        choixChamp4[4] = new JComboBox();
        choixChamp4[4].addItem("Numero");
        choixChamp4[4].addItem("Code_service");
        choixChamp4[4].addItem("Rotation");
        choixChamp4[4].addItem("Salaire");
        
        choixChamp4[5] = new JComboBox();
        choixChamp4[5].addItem("Numero");
        choixChamp4[5].addItem("Nom");
        choixChamp4[5].addItem("Prenom");
        choixChamp4[5].addItem("Adresse");
        choixChamp4[5].addItem("Tel");
        choixChamp4[5].addItem("Mutuelle");
        
        choixChamp4[6] = new JComboBox();
        choixChamp4[6].addItem("No_malade");
        choixChamp4[6].addItem("Code_service");
        choixChamp4[6].addItem("No_chambre");
        choixChamp4[6].addItem("Lit");
        
        choixChamp4[7] = new JComboBox();
        choixChamp4[7].addItem("No_Docteur");
        choixChamp4[7].addItem("No_Malade");
        
        choixCondition.addItem("Oui");
        choixCondition.addItem("Non");
        
        
        //Instanciation du Label
        Affi1 = new JLabel("Souhaitez vous ajouter une ou plusieurs conditions ?");
        Affi2 = new JLabel ("Condition : ");
        Affi3 = new JLabel (" = ");
        Affi4 = new JLabel (" = ");
        Affi5 = new JLabel (" = ");
        
        //Création des action listeners
        choixTable.addItemListener(new ItemState());
        choixTable.addActionListener(new ItemAction2());
        nombreCondition.addActionListener(new ItemAction2());
        text1.addActionListener(new ItemAction2());
        text2.addActionListener(new ItemAction2());
        text3.addActionListener(new ItemAction2());
        choixCondition.addActionListener(new ItemAction2());
        AffichageFenetre.setPreferredSize(new Dimension(800, 500));
        
        //Ajout d'un Action Listener sur chacune des JComboBox
        for(int i=0;i<8;i++)
        {
            choixChamp[i].addActionListener(new ItemAction2());
        }
        
        for(int i=0;i<8;i++)
        {
            choixChamp2[i].addActionListener(new ItemAction2());
        }
        
        for(int i=0;i<8;i++)
        {
            choixChamp3[i].addActionListener(new ItemAction2());
        }
        
        for(int i=0;i<8;i++)
        {
            choixChamp4[i].addActionListener(new ItemAction2());
        }
        valider2.addActionListener(new ItemAction2());
        requete.add(choixTable);
        requete.add(choixChamp[0]);
        //Réglage de la taille du champ de saisie de texte
        text1.setPreferredSize(new Dimension(150, 40));
        text2.setPreferredSize(new Dimension(150, 40));
        text3.setPreferredSize(new Dimension(150, 40));
        //Ajout des labels
        requete.add(Affi1);
        //Ajout de la combo box de choix fermé
        requete.add(choixCondition);
        requete.add(nombreCondition);
        requete.add(Affi2);
        requete.add(choixChamp2[0]);
        
        requete.add(Affi3);
        //Ajout du champs de saisie de texte
        requete.add(text1);
        //Ajout du bouton valider
        requete.add(valider2);

        

        ///Creation de l'onglet Modification*******************************************************************************
        JLabel titreOnglet3 = new JLabel("Modification"); ///Instancition de l'onglet modification
        modif.add(titreOnglet3);
        modif.setPreferredSize(new Dimension(1000, 780));
        modif.setBackground(Color.white); ///Couleur de fond de l'onglet 3
        onglets.add("Modification", modif);
        ///Creation des menus déroulants
        choixModif.setPreferredSize(new Dimension(150, 20)); ///Definition du menu déroulant pour choisir l'option de modification
        ///Ajout des différentes options
        choixModif.addItem("Modifier");
        choixModif.addItem("Inserer");
        choixModif.addItem("Supprimer");
        ///Ajout des listeners au menu deroulant choixModif
        choixModif.addItemListener(new ItemState());
        choixModif.addActionListener(new ItemAction());
        ///Ajout des différentes tables modifiables
        choixTab.setPreferredSize(new Dimension(150, 20)); ///Definition du menu déroulant pour choisir l'option de modification
        ///Ajout des différents element de choixTab
        ///Service
        choixTab.addItem("Service");
        ///Modification tailles textes
        codeService.setPreferredSize(new Dimension(130, 20));
        codeListeService.setPreferredSize(new Dimension(130, 20));
        ///Ajout des différents éléments de CodeListeservice
        ArrayList<String> code_services; ///Variables utiles
        code_services = new ArrayList<String>();
        try { ///Creation d'une connexion  
            Connexion co = new Connexion("projethopital", "root", "");
            code_services = co.remplirChampsRequete("SELECT code FROM service"); ///Creation de la requete permettant de récupérer les différentes clées primaire de la classe service
            } catch (SQLException | ClassNotFoundException ex) {
                Logger.getLogger(FentreP.class.getName()).log(Level.SEVERE, null, ex);
            }
        ///Ajoute les clées primaire au menu deroulant 
        for(int i=0; i<code_services.size(); i++)
        {
            codeListeService.addItem(code_services.get(i));
        }
        nomService.setPreferredSize(new Dimension(130, 20));
        batimentService.setPreferredSize(new Dimension(130, 20));
        directeurService.setPreferredSize(new Dimension(130, 20));
        ///Chambre
        choixTab.addItem("Chambre");
        ///Modification tailles textes
        code_serviceChambre.setPreferredSize(new Dimension(130, 20));
        ArrayList<String> no_chambre; ///Variables utiles
        no_chambre = new ArrayList<String>();
        try { ///Creation d'une connexion  
            Connexion co = new Connexion("projethopital", "root", "");
            no_chambre = co.remplirChampsRequete("SELECT no_chambre FROM chambre"); ///Creation de la requete permettant de récupérer une clée primaire de la classe chabre
            } catch (SQLException | ClassNotFoundException ex) {
                Logger.getLogger(FentreP.class.getName()).log(Level.SEVERE, null, ex);
            }
        ///Ajoute les clées primaire au menu deroulant 
        for(int i=0; i<no_chambre.size(); i++)
        {
            no_chambreListeChambre.addItem(no_chambre.get(i));
        }
        code_serviceListeChambre.setPreferredSize(new Dimension(130, 20));
        ArrayList<String> code_serviceChambre; ///Variables utiles
        code_serviceChambre = new ArrayList<String>();
        try { ///Creation d'une connexion  
            Connexion co = new Connexion("projethopital", "root", "");
            code_serviceChambre = co.remplirChampsRequete("SELECT code_service FROM chambre"); ///Creation de la requete permettant de récupérer une clée primaire de la classe chabre
            } catch (SQLException | ClassNotFoundException ex) {
                Logger.getLogger(FentreP.class.getName()).log(Level.SEVERE, null, ex);
            }
        ///Ajoute les clées primaire au menu deroulant 
        for(int i=0; i<no_chambre.size(); i++)
        {
            code_serviceListeChambre.addItem(no_chambre.get(i));
        }
        no_chambreChambre.setPreferredSize(new Dimension(130, 20));
        surveillantChambre.setPreferredSize(new Dimension(130, 20));
        nb_litsChambre.setPreferredSize(new Dimension(130, 20));
        ///Employee
        choixTab.addItem("Employe");
        ///Modification tailles textes
        numeroEmployee.setPreferredSize(new Dimension(130, 20));
        ArrayList<String> numeroEmployeeO; ///Variables utiles
        numeroEmployeeO = new ArrayList<String>();
        try { ///Creation d'une connexion  
            Connexion co = new Connexion("projethopital", "root", "");
            numeroEmployeeO = co.remplirChampsRequete("SELECT numero FROM employe"); ///Creation de la requete permettant de récupérer une clée primaire de la classe chabre
            } catch (SQLException | ClassNotFoundException ex) {
                Logger.getLogger(FentreP.class.getName()).log(Level.SEVERE, null, ex);
            }
        ///Ajoute les clées primaire au menu deroulant 
        for(int i=0; i<no_chambre.size(); i++)
        {
            numeroListeEmployee.addItem(numeroEmployeeO.get(i));
        }
        nomEmployee.setPreferredSize(new Dimension(130, 20));
        prenomEmployee.setPreferredSize(new Dimension(130, 20));
        adresseEmployee.setPreferredSize(new Dimension(130, 20));
        telEmployee.setPreferredSize(new Dimension(130, 20));
        ///Docteur
        choixTab.addItem("Docteur");
        ///Modification tailles textes
        numeroDocteur.setPreferredSize(new Dimension(130, 20));
        numeroListeEmployee.setPreferredSize(new Dimension(130, 20)); ///liste des différents clé primaire existante pour la table chambre
        ArrayList<String> numeroDocteurO; ///Variables utiles
        numeroDocteurO = new ArrayList<String>();
        try { ///Creation d'une connexion  
            Connexion co = new Connexion("projethopital", "root", "");
            numeroDocteurO = co.remplirChampsRequete("SELECT numero FROM docteur"); ///Creation de la requete permettant de récupérer une clée primaire de la classe chabre
            } catch (SQLException | ClassNotFoundException ex) {
                Logger.getLogger(FentreP.class.getName()).log(Level.SEVERE, null, ex);
            }
        ///Ajoute les clées primaire au menu deroulant 
        for(int i=0; i<no_chambre.size(); i++)
        {
            numeroListeDocteur.addItem(numeroDocteurO.get(i));
        }
        specialiteDocteur.setPreferredSize(new Dimension(130, 20));
        ///Infirmier
        choixTab.addItem("Infirmier");
        ///Modification tailles textes
        numeroInfirmier.setPreferredSize(new Dimension(130, 20));
        numeroListeInfirmier.setPreferredSize(new Dimension(130, 20));
        ArrayList<String> numeroInfirmierO; ///Variables utiles
        numeroInfirmierO = new ArrayList<String>();
        try { ///Creation d'une connexion  
            Connexion co = new Connexion("projethopital", "root", "");
            numeroInfirmierO = co.remplirChampsRequete("SELECT numero FROM infirmier"); ///Creation de la requete permettant de récupérer une clée primaire de la classe chabre
            } catch (SQLException | ClassNotFoundException ex) {
                Logger.getLogger(FentreP.class.getName()).log(Level.SEVERE, null, ex);
            }
        ///Ajoute les clées primaire au menu deroulant 
        for(int i=0; i<no_chambre.size(); i++)
        {
            numeroListeInfirmier.addItem(numeroInfirmierO.get(i));
        }
        code_serviceInfirmier.setPreferredSize(new Dimension(130, 20));
        rotationInfirmier.setPreferredSize(new Dimension(130, 20));
        rotationInfirmier.addItem("JOUR");
        rotationInfirmier.addItem("NUIT");
        salaireInfirmier.setPreferredSize(new Dimension(130, 20));
        
        choixTab.addItem("Malade");
        ///Hospitalisation
         numeroMalade.setPreferredSize(new Dimension(130, 20));
         ArrayList<String> numeroMaladeO; ///Variables utiles
        numeroMaladeO = new ArrayList<String>();
        try { ///Creation d'une connexion  
            Connexion co = new Connexion("projethopital", "root", "");
            numeroMaladeO = co.remplirChampsRequete("SELECT numero FROM malade"); ///Creation de la requete permettant de récupérer une clée primaire de la classe chabre
            } catch (SQLException | ClassNotFoundException ex) {
                Logger.getLogger(FentreP.class.getName()).log(Level.SEVERE, null, ex);
            }
        ///Ajoute les clées primaire au menu deroulant 
        for(int i=0; i<numeroMaladeO.size(); i++)
        {
            numeroListeMalade.addItem(numeroMaladeO.get(i));
        }
         nomMalade.setPreferredSize(new Dimension(130, 20));
         prenomMalade.setPreferredSize(new Dimension(130, 20));
         adresseMalade.setPreferredSize(new Dimension(130, 20));
         telMalade.setPreferredSize(new Dimension(130, 20));
         mutuelleMalade.setPreferredSize(new Dimension(130, 20));
         
         choixTab.addItem("Hospitalisation");
        ///Modification tailles textes
        no_maladeHospitalisation.setPreferredSize(new Dimension(130, 20));
        no_maladeListeHospitalisation.setPreferredSize(new Dimension(130, 20));
        ArrayList<String> no_maladeHospitalisationO; ///Variables utiles
        no_maladeHospitalisationO = new ArrayList<String>();
        try { ///Creation d'une connexion  
            Connexion co = new Connexion("projethopital", "root", "");
            no_maladeHospitalisationO = co.remplirChampsRequete("SELECT no_malade FROM hospitalisation"); ///Creation de la requete permettant de récupérer une clée primaire de la classe chabre
            } catch (SQLException | ClassNotFoundException ex) {
                Logger.getLogger(FentreP.class.getName()).log(Level.SEVERE, null, ex);
            }
        ///Ajoute les clées primaire au menu deroulant 
        for(int i=0; i<no_maladeHospitalisationO.size(); i++)
        {
            no_maladeListeHospitalisation.addItem(no_maladeHospitalisationO.get(i));
        }
        code_serviceHospitalisation.setPreferredSize(new Dimension(130, 20));
        no_chambreHospitalisation.setPreferredSize(new Dimension(130, 20));
        litHospitalisation.setPreferredSize(new Dimension(130, 20));
        ///Soignee
        choixTab.addItem("Soignee");
        ///Modification tailles textes
        no_docteurSoignee.setPreferredSize(new Dimension(130, 20));
        
        no_maladeSoignee.setPreferredSize(new Dimension(130, 20));
        ArrayList<String> no_docteurSoigneeO; ///Variables utiles
        no_docteurSoigneeO = new ArrayList<String>();
        try { ///Creation d'une connexion  
            Connexion co = new Connexion("projethopital", "root", "");
            no_docteurSoigneeO = co.remplirChampsRequete("SELECT no_malade FROM hospitalisation"); ///Creation de la requete permettant de récupérer une clée primaire de la classe chabre
            } catch (SQLException | ClassNotFoundException ex) {
                Logger.getLogger(FentreP.class.getName()).log(Level.SEVERE, null, ex);
            }
        ///Ajoute les clées primaire au menu deroulant 
        for(int i=0; i<no_docteurSoigneeO.size(); i++)
        {
            no_docteurListeSoignee.addItem(no_docteurSoigneeO.get(i));
        }
        ///Ajout les listeners au menu deroulant choixTab
        choixTab.addItemListener(new ItemState());
        choixTab.addActionListener(new ItemAction());
        
        ///Ajout des listeners de boite texte
        ///Service
        codeService.addActionListener(new ItemAction());
        codeListeService.addActionListener(new ItemAction());
        nomService.addActionListener(new ItemAction());
        batimentService.addActionListener(new ItemAction());
        directeurService.addActionListener(new ItemAction());
        ///Chambre
        ///code_serviceChambre.addActionListener(new ItemAction());
        no_chambreChambre.addActionListener(new ItemAction());
        no_chambreListeChambre.addActionListener(new ItemAction());
        surveillantChambre.addActionListener(new ItemAction());
        nb_litsChambre.addActionListener(new ItemAction());
        ///Employee
        numeroEmployee.addActionListener(new ItemAction());
        numeroListeEmployee.addActionListener(new ItemAction());
        nomEmployee.addActionListener(new ItemAction());
        prenomEmployee.addActionListener(new ItemAction());
        adresseEmployee.addActionListener(new ItemAction());
        telEmployee.addActionListener(new ItemAction());
        ///Docteur
        numeroDocteur.addActionListener(new ItemAction());
        specialiteDocteur.addActionListener(new ItemAction());
        numeroListeDocteur.addActionListener(new ItemAction());
        ///Infirmier
        numeroInfirmier.addActionListener(new ItemAction());
        numeroListeInfirmier.addActionListener(new ItemAction());
        code_serviceInfirmier.addActionListener(new ItemAction());
        rotationInfirmier.addActionListener(new ItemAction());
        salaireInfirmier.addActionListener(new ItemAction());
        
        ///Malade
        numeroListeMalade.addActionListener(new ItemAction());
        
        ///Hospitalisation
        no_maladeListeHospitalisation.addActionListener(new ItemAction());
        
        ///Soignee
        no_docteurListeSoignee.addActionListener(new ItemAction());
                
        ///Ajout du listener au bouton valider
        valider.addActionListener(new ItemAction());    
        
        ///Ajout de tout les panels dans la fenetre et methodes pour la rendre visible
        modif.add(choixModif); ///Ajout du menu déroulant de choix de modification au panel modification
        modif.add(choixTab); ///Ajout du menu déroulant de choix de la table à modifier au panel modification
        onglets.setOpaque(true);
        panelP.add(onglets);
        this.getContentPane().add(panelP);
        this.setVisible(true);
        
        
    }
    
    ///Acesseurs******************************************************************************
    
    
    ///Methodes*******************************************************************************
    
    
    ///Methodes Listener***********************************************************************
    @Override
    public void actionPerformed(ActionEvent e) 
    {
       
    }
    
    class ItemState implements ItemListener
    {
        @Override
        public void itemStateChanged(ItemEvent e) 
        {
            System.out.println("événement déclenché sur : " + e.getItem());
        }               
    }
    
    class ItemAction2 implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent e) 
        {
            int j=0, tailleTab=1, tailleTab2=0,m=0,n=0,repere=0, compte=0;
            String tempString;
            
            if(choixTable.getSelectedItem()=="Service"){j=0;}
            if(choixTable.getSelectedItem()=="Chambre"){j=1;}
            if(choixTable.getSelectedItem()=="Employe"){j=2;}
            if(choixTable.getSelectedItem()=="Docteur"){j=3;}
            if(choixTable.getSelectedItem()=="Infirmier"){j=4;}
            if(choixTable.getSelectedItem()=="Malade"){j=5;}
            if(choixTable.getSelectedItem()=="Hospitalisation"){j=6;}
            if(choixTable.getSelectedItem()=="Soigne"){j=7;System.out.println("Test 7");}
            
            col=new String[1];
            col[0]=choixChamp[j].getSelectedItem().toString();
            
            if(e.getSource()==valider2) ///Choix de la table service
            {
                if(choixChamp[j].getSelectedItem()=="*")
                {
                    if(j==0){tailleTab=4; col = new String[tailleTab];
                    col[0]="Code";col[1]="Nom";col[2]="Batiment";col[3]="Directeur";}
                    if(j==1){tailleTab=4; col = new String[tailleTab];
                    col[0]="Code_service";col[1]="No_chambre";col[2]="Surveillant";col[3]="Nb_lits";}
                    if(j==2){tailleTab=5; col = new String[tailleTab];
                    col[0]="Numero";col[1]="Nom";col[2]="Prenom";col[3]="Adresse";col[4]="tel";}
                    if(j==3){tailleTab=2; col = new String[tailleTab];
                    col[0]="Numero";col[1]="Specialite";}
                    if(j==4){tailleTab=4; col = new String[tailleTab];
                    col[0]="Numero";col[1]="Code_service";col[2]="Rotation";col[3]="Salaire";}
                    if(j==5){tailleTab=6; col = new String[tailleTab];
                    col[0]="Numero";col[1]="Nom";col[2]="Prenom";col[3]="Adresse";col[4]="Tel";col[5]="Mutuelle";}
                    if(j==6){tailleTab=4; col = new String[tailleTab];
                    col[0]="No_malade";col[1]="Code_service";col[2]="No_chambre";col[3]="Lit";}
                    if(j==7){tailleTab=2; col = new String[tailleTab];
                    col[0]="No_docteur";col[1]="No_malade";}
                    
                }
                
                DefaultTableModel tableModel = new DefaultTableModel(col, 0);
                
                try {
                    Connexion co = new Connexion("projethopital", "root", "");
                    if(choixCondition.getSelectedItem()=="Non")
                    {
                        System.out.println("SELECT "+choixChamp[j].getSelectedItem()+" FROM "+choixTable.getSelectedItem());
                        System.out.println(co.remplirChampsRequete("SELECT "+choixChamp[j].getSelectedItem()+" FROM "+choixTable.getSelectedItem()));
                        /*for(m=0;m<co.remplirChampsRequete("SELECT "+choixChamp[j].getSelectedItem()+" FROM "+choixTable.getSelectedItem()).size();m++);
                        {
                            for(n=0;n<tailleTab;n++)
                            {
                                //char String tempString = new String;
                                data[m][n]= co.remplirChampsRequete("SELECT "+choixChamp[j].getSelectedItem()+" FROM "+choixTable.getSelectedItem()).get(m).toString();
                                //System.out.println(data);
                            }
                        }*/
                        tailleTab2=co.remplirChampsRequete("SELECT "+choixChamp[j].getSelectedItem()+" FROM "+choixTable.getSelectedItem()).size();
                        
                        for(m=0;m<tailleTab2;m++)
                        {
                            repere=0;
                            compte=0;
                            tempString = co.remplirChampsRequete("SELECT "+choixChamp[j].getSelectedItem()+" FROM "+choixTable.getSelectedItem()).get(m).toString();
                            for(n=0;n<tempString.length();n++)
                            {
                                if((tempString.charAt(n)==',')&&(tempString.charAt(n+1)!=' '))
                                {
                                    tableauObj[compte][m] = new String(tempString.substring(repere,n));
                                    repere=n+1;
                                    data[compte]=tableauObj[compte][m];
                                    compte=compte+1;
                                }
                            }
                            tableauObj[compte][m]=tempString.substring(repere,n);
                            data[compte]=tableauObj[compte][m];
                            tableModel.addRow(data);
                            
                            //data = new String[] { tableauObj[n][m], tableauObj[n][m], tableauObj[n][m], tableauObj[n][m] };
                            //tableModel.addRow(tableauObj);
                        }
                        
                        //requete.add(new JTable(tableModel), BorderLayout.CENTER);
                            JScrollPane PanneauD = new JScrollPane(new JTable(tableModel));
                            PanneauD.setPreferredSize(new Dimension(800,600));
                            requete.add(PanneauD);

                        /*for(m=0;m<tailleTab2;m++)
                        {
                            requete.add(new JLabel(co.remplirChampsRequete("SELECT "+choixChamp[j].getSelectedItem()+" FROM "+choixTable.getSelectedItem()).get(m).toString()));
                            requete.add(new JLabel("-------------------------------------------------------------------------------------------------------------------------------------"));
                            
                        }*/
                    }
                    else 
                    {
                        
                        if(nombreCondition.getSelectedItem()=="1")
                        {
                            tailleTab2=co.remplirChampsRequete("SELECT "+choixChamp[j].getSelectedItem()+" FROM "+choixTable.getSelectedItem()+" WHERE "+choixChamp2[j].getSelectedItem()+" = '"+text1.getText()+"'").size();
                            for(m=0;m<tailleTab2;m++)
                            {
                                repere=0;
                                compte=0;
                                tempString = co.remplirChampsRequete("SELECT "+choixChamp[j].getSelectedItem()+" FROM "+choixTable.getSelectedItem()+" WHERE "+choixChamp2[j].getSelectedItem()+" = '"+text1.getText()+"'").get(m).toString();
                                for(n=0;n<tempString.length();n++)
                                {
                                    if((tempString.charAt(n)==',')&&(tempString.charAt(n+1)!=' '))
                                    {
                                        tableauObj[compte][m] = new String(tempString.substring(repere,n));
                                        repere=n+1;
                                        data[compte]=tableauObj[compte][m];
                                        compte=compte+1;
                                    }
                                }
                                tableauObj[compte][m]=tempString.substring(repere,n);
                                data[compte]=tableauObj[compte][m];
                                tableModel.addRow(data);

                                //data = new String[] { tableauObj[n][m], tableauObj[n][m], tableauObj[n][m], tableauObj[n][m] };
                                //tableModel.addRow(tableauObj);
                            }
                            JScrollPane PanneauD = new JScrollPane(new JTable(tableModel));
                            PanneauD.setPreferredSize(new Dimension(800,600));
                            requete.add(PanneauD);
                            System.out.println("SELECT "+choixChamp[j].getSelectedItem()+" FROM "+choixTable.getSelectedItem()+" WHERE "+choixChamp2[j].getSelectedItem()+" = '"+text1.getText()+"'");
                            System.out.println(co.remplirChampsRequete("SELECT "+choixChamp[j].getSelectedItem()+" FROM "+choixTable.getSelectedItem()+" WHERE "+choixChamp2[j].getSelectedItem()+" = '"+text1.getText()+"'"));
                        }
                        
                        if(nombreCondition.getSelectedItem()=="2")
                        {
                            tailleTab2=co.remplirChampsRequete("SELECT "+choixChamp[j].getSelectedItem()+" FROM "+choixTable.getSelectedItem()+" WHERE "+choixChamp2[j].getSelectedItem()+" = '"+text1.getText()+"' AND "+choixChamp3[j].getSelectedItem()+" = '"+text2.getText()+"'").size();
                            for(m=0;m<tailleTab2;m++)
                            {
                                repere=0;
                                compte=0;
                                tempString = co.remplirChampsRequete("SELECT "+choixChamp[j].getSelectedItem()+" FROM "+choixTable.getSelectedItem()+" WHERE "+choixChamp2[j].getSelectedItem()+" = '"+text1.getText()+"' AND "+choixChamp3[j].getSelectedItem()+" = '"+text2.getText()+"'").get(m).toString();
                                for(n=0;n<tempString.length();n++)
                                {
                                    if((tempString.charAt(n)==',')&&(tempString.charAt(n+1)!=' '))
                                    {
                                        tableauObj[compte][m] = new String(tempString.substring(repere,n));
                                        repere=n+1;
                                        data[compte]=tableauObj[compte][m];
                                        compte=compte+1;
                                    }
                                }
                                tableauObj[compte][m]=tempString.substring(repere,n);
                                data[compte]=tableauObj[compte][m];
                                tableModel.addRow(data);

                                //data = new String[] { tableauObj[n][m], tableauObj[n][m], tableauObj[n][m], tableauObj[n][m] };
                                //tableModel.addRow(tableauObj);
                            }
                            JScrollPane PanneauD = new JScrollPane(new JTable(tableModel));
                            PanneauD.setPreferredSize(new Dimension(800,600));
                            requete.add(PanneauD);
                            System.out.println("SELECT "+choixChamp[j].getSelectedItem()+" FROM "+choixTable.getSelectedItem()+" WHERE "+choixChamp2[j].getSelectedItem()+" = '"+text1.getText()+"' AND "+choixChamp3[j].getSelectedItem()+" = '"+text2.getText()+"'");
                            System.out.println(co.remplirChampsRequete("SELECT "+choixChamp[j].getSelectedItem()+" FROM "+choixTable.getSelectedItem()+" WHERE "+choixChamp2[j].getSelectedItem()+" = '"+text1.getText()+"' AND "+choixChamp3[j].getSelectedItem()+" = '"+text2.getText()+"'"));
                        }
                        
                        if(nombreCondition.getSelectedItem()=="3")
                        {
                            tailleTab2=co.remplirChampsRequete("SELECT "+choixChamp[j].getSelectedItem()+" FROM "+choixTable.getSelectedItem()+" WHERE "+choixChamp2[j].getSelectedItem()+" = '"+text1.getText()+"' AND "+choixChamp3[j].getSelectedItem()+" = '"+text2.getText()+"' AND "+choixChamp4[j].getSelectedItem()+" = '"+text3.getText()+"'").size();
                            for(m=0;m<tailleTab2;m++)
                            {
                                repere=0;
                                compte=0;
                                tempString = co.remplirChampsRequete("SELECT "+choixChamp[j].getSelectedItem()+" FROM "+choixTable.getSelectedItem()+" WHERE "+choixChamp2[j].getSelectedItem()+" = '"+text1.getText()+"' AND "+choixChamp3[j].getSelectedItem()+" = '"+text2.getText()+"' AND "+choixChamp4[j].getSelectedItem()+" = '"+text3.getText()+"'").get(m).toString();
                                for(n=0;n<tempString.length();n++)
                                {
                                    if((tempString.charAt(n)==',')&&(tempString.charAt(n+1)!=' '))
                                    {
                                        tableauObj[compte][m] = new String(tempString.substring(repere,n));
                                        repere=n+1;
                                        data[compte]=tableauObj[compte][m];
                                        compte=compte+1;
                                    }
                                }
                                tableauObj[compte][m]=tempString.substring(repere,n);
                                data[compte]=tableauObj[compte][m];
                                tableModel.addRow(data);

                                //data = new String[] { tableauObj[n][m], tableauObj[n][m], tableauObj[n][m], tableauObj[n][m] };
                                //tableModel.addRow(tableauObj);
                            }
                            JScrollPane PanneauD = new JScrollPane(new JTable(tableModel));
                            PanneauD.setPreferredSize(new Dimension(800,600));
                            requete.add(PanneauD);
                            System.out.println("SELECT "+choixChamp[j].getSelectedItem()+" FROM "+choixTable.getSelectedItem()+" WHERE "+choixChamp2[j].getSelectedItem()+" = '"+text1.getText()+"' AND "+choixChamp3[j].getSelectedItem()+" = '"+text2.getText()+"' AND "+choixChamp4[j].getSelectedItem()+" = '"+text3.getText()+"'");
                            System.out.println(co.remplirChampsRequete("SELECT "+choixChamp[j].getSelectedItem()+" FROM "+choixTable.getSelectedItem()+" WHERE "+choixChamp2[j].getSelectedItem()+" = '"+text1.getText()+"' AND "+choixChamp3[j].getSelectedItem()+" = '"+text2.getText()+"' AND "+choixChamp4[j].getSelectedItem()+" = '"+text3.getText()+"'"));
                        }
                    }
                    
                    
                } catch (SQLException | ClassNotFoundException ex) {
                    Logger.getLogger(FentreP.class.getName()).log(Level.SEVERE, null, ex);
                }
                //requete.add(AffichageFenetre);
            }
            
            if(e.getSource()!=valider2)
            {
                DefaultTableModel tableModel = new DefaultTableModel(col, 0);
                System.out.println(j);
                ///Efface le contenue du panel
                requete.removeAll();
                text1.setPreferredSize(new Dimension(0, 0));
                requete.add(choixTable);
                //Ajout de la combo box
                requete.add(choixChamp[j]);
                //Ajout du label
                requete.add(Affi1);
                //Ajout de la combo box de choix fermé
                requete.add(choixCondition);
                
                //Si l'utilisateur souhaite trier selon une ou plusieurs condition condition
                if(choixCondition.getSelectedItem()=="Oui")
                {
                    text1.setPreferredSize(new Dimension(150, 40));
                    requete.add(nombreCondition);
                    requete.add(Affi2);
                    requete.add(choixChamp2[j]);
                    requete.add(Affi3);
                    //Ajout du champ de saisie de texte
                    requete.add(text1);
                    if(nombreCondition.getSelectedItem()=="2")
                    {
                        requete.add(choixChamp3[j]);
                        requete.add(Affi4);
                        requete.add(text2);
                    }
                    else if(nombreCondition.getSelectedItem()=="3")
                    {
                        requete.add(choixChamp3[j]);
                        requete.add(Affi4);
                        requete.add(text2);
                        
                        requete.add(choixChamp4[j]);
                        requete.add(Affi5);
                        requete.add(text3);
                    }
                }

                requete.add(valider2);
                
            }
        }
    }
        
        //Actions listeners de requête *************************************************************************
        //*************************************************************************************************************************
        
    class ItemAction implements ActionListener
    {


        @Override
        public void actionPerformed(ActionEvent e) 
        {
            ///Modification*************************************************************************************************************************************************************
            ///service
            if(choixModif.getSelectedItem() == "Modifier" && choixTab.getSelectedItem() == "Service" && e.getSource() == valider)
            {
                //Modification des valeurs par celles entré par l'utilisateur
                    requeteF = "nom = '" +  nomService.getText() + "' , batiment = '" + batimentService.getText() + "' ,directeur = " + directeurService.getText() + " WHERE code = '" + codeListeService.getSelectedItem().toString().substring(0, codeListeService.getSelectedItem().toString().length()-1) + "'";
                    String requete;
                    requete = "UPDATE " + choixTab.getSelectedItem().toString() + " SET " + requeteF ;
                    System.out.println(requete);
                    try {
                        Connexion co = new Connexion("projethopital", "root", "");
                        co.executeUpdate(requete);
                    } catch (SQLException | ClassNotFoundException ex) {
                            Logger.getLogger(FentreP.class.getName()).log(Level.SEVERE, null, ex);
                    }
                        requeteF = "";
            }
            else if(choixModif.getSelectedItem() == "Modifier" && choixTab.getSelectedItem() == "Service")
            {
                    ///Efface le contenue du panel
                    modif.removeAll();
                    modif.add(choixModif);
                    modif.add(choixTab);
                    modif.add(labelCodeService);
                    modif.add(codeListeService);
                 
                    ///Recuperation du codeListeService
                    String recup = new String(); ///string servant à modifier du texte sans l'altérer
                    recup = codeListeService.getSelectedItem().toString();
                    recup = recup.substring(0, recup.length()-1);
                    System.out.println("Code: " + recup);
                
                    ///Etablissement des différentes requetes
                    String requete1 = "SELECT nom FROM service WHERE code = '" + recup + "'";
                    String requete2 = "SELECT batiment FROM service WHERE code = '" + recup+ "'";
                    String requete3 = "SELECT directeur FROM service WHERE code = '" + recup + "'";
                    System.out.println(requete1);
                    System.out.println(requete2);
                    System.out.println(requete3);
                    try {
                        Connexion co = new Connexion("projethopital", "root", "");
                        recup = co.remplirChampsRequete(requete1).toString(); ///Donne la valeur de la requete
                        ///Enleve les crochets
                        recup = recup.substring(1, recup.length()-1);
                        System.out.println("Nom: " + recup);
                        nomService.setText(recup);
                        recup = co.remplirChampsRequete(requete2).toString();
                        recup = recup.substring(1, recup.length()-1);
                        System.out.println("Batiment: " + recup);
                        batimentService.setText(recup);
                        recup = co.remplirChampsRequete(requete3).toString();
                        recup = recup.substring(1, recup.length()-2);
                        System.out.println("Directeur: " + recup);
                        directeurService.setText(recup);

                    } catch (SQLException | ClassNotFoundException ex) {
                        Logger.getLogger(FentreP.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    
                    modif.add(labelNomService);
                    modif.add(nomService);
                    modif.add(labelBatimentService);
                    modif.add(batimentService);
                    modif.add(labelDirecteurService);
                    modif.add(directeurService);
                    modif.add(valider);
            }
            
            ///Chambre
            if(choixModif.getSelectedItem() == "Modifier" && choixTab.getSelectedItem() == "Chambre" && e.getSource() == valider)
            {
                //Modification des valeurs par celles entré par l'utilisateur
                    requeteF = "surveillant = " +  surveillantChambre.getText() + " , nb_lits = " +nb_litsChambre.getText() + " WHERE no_chambre = " + no_chambreListeChambre.getSelectedItem().toString() + " AND code_service = '" + codeListeService.getSelectedItem().toString().substring(0, codeListeService.getSelectedItem().toString().length()-1) + "'";
                    String requete;
                    requete = "UPDATE " + choixTab.getSelectedItem().toString() + " SET " + requeteF ;
                    System.out.println(requete);
                    try {
                        Connexion co = new Connexion("projethopital", "root", "");
                        co.executeUpdate(requete);
                    } catch (SQLException | ClassNotFoundException ex) {
                            Logger.getLogger(FentreP.class.getName()).log(Level.SEVERE, null, ex);
                    }
                        requeteF = "";
            }
            else if(choixModif.getSelectedItem() == "Modifier" && choixTab.getSelectedItem() == "Chambre")
            {
                    ///Efface le contenue du panel
                    modif.removeAll();
                    modif.add(choixModif);
                    modif.add(choixTab);
                    modif.add(labelNo_chambreChambre);
                    modif.add(no_chambreListeChambre);
                    modif.add(labelCodeService);
                    modif.add(codeListeService);
                 
                    ///Recuperation du codeListeService
                    String recup = new String(); ///string servant à modifier du texte sans l'altérer
                    String recup2 = new String();
                    recup = no_chambreListeChambre.getSelectedItem().toString();
                    recup = recup.substring(0, recup.length()-1);
                    recup2 = codeListeService.getSelectedItem().toString();
                    recup2 = recup2.substring(0, recup2.length()-1);
                    System.out.println("Recup: " + recup);
                    System.out.println("Recup2: " + recup2);
                                        
                    ///Etablissement des différentes requetes
                    String requete1 = "SELECT surveillant FROM chambre WHERE no_chambre = " + recup + " AND code_service = '" + recup2 + "'";
                    String requete2 = "SELECT nb_lits FROM chambre WHERE no_chambre = " + recup + " AND code_service = '" + recup2 + "'";
                    System.out.println("Requet1: " + requete1);
                    System.out.println("Requet2: " + requete2);

                    try {
                        Connexion co = new Connexion("projethopital", "root", "");
                        recup = co.remplirChampsRequete(requete1).toString(); ///Donne la valeur de la requete
                        recup = recup.substring(1, recup.length()-2); ///Enleve les crochets
                        System.out.println("Resultat requete1: " + recup);
                        surveillantChambre.setText(recup);
                        recup = co.remplirChampsRequete(requete2).toString();
                        recup = recup.substring(1, recup.length()-2);
                        System.out.println("Resultat requete2: " + recup);
                        nb_litsChambre.setText(recup);

                    } catch (SQLException | ClassNotFoundException ex) {
                        Logger.getLogger(FentreP.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    
                    modif.add(labelSurveillantChambre);
                    modif.add(surveillantChambre);
                    modif.add(labelNb_litsChambre);
                    modif.add(nb_litsChambre);
                    modif.add(valider);
            }
            ///Employee
            if(choixModif.getSelectedItem() == "Modifier" && choixTab.getSelectedItem() == "Employe" && e.getSource() == valider)
            {
                //Modification des valeurs par celles entré par l'utilisateur
                    requeteF = "nom = '" +  nomEmployee.getText() + "' , prenom = '" +prenomEmployee.getText() + "' , adresse = '" + adresseEmployee.getText() + "' , tel = '" + telEmployee.getText() + "' WHERE numero = " + numeroListeEmployee.getSelectedItem().toString();
                    String requete;
                    requete = "UPDATE " + choixTab.getSelectedItem().toString() + " SET " + requeteF ;
                    System.out.println(requete);
                    try {
                        Connexion co = new Connexion("projethopital", "root", "");
                        co.executeUpdate(requete);
                    } catch (SQLException | ClassNotFoundException ex) {
                            Logger.getLogger(FentreP.class.getName()).log(Level.SEVERE, null, ex);
                    }
                        requeteF = "";
            }
            else if(choixModif.getSelectedItem() == "Modifier" && choixTab.getSelectedItem() == "Employe")
            {
                    ///Efface le contenue du panel
                    modif.removeAll();
                    modif.add(choixModif);
                    modif.add(choixTab);
                    modif.add(labelNumeroEmployee);
                    modif.add(numeroListeEmployee);
                 
                    ///Recuperation du codeListeService
                    String recup = new String(); ///string servant à modifier du texte sans l'altérer
                    recup = numeroListeEmployee.getSelectedItem().toString();
                    recup = recup.substring(0, recup.length()-1);
                    System.out.println("Recup: " + recup);
                                        
                    ///Etablissement des différentes requetes
                    String requete1 = "SELECT nom FROM employe WHERE numero = " + recup;
                    String requete2 = "SELECT prenom FROM employe WHERE numero = " + recup;
                    String requete3 = "SELECT adresse FROM employe WHERE numero = " + recup;
                    String requete4 = "SELECT tel FROM employe WHERE numero = " + recup;
                    System.out.println("Requet1: " + requete1);
                    System.out.println("Requet2: " + requete2);
                    System.out.println("Requet3: " + requete3);
                    System.out.println("Requet4: " + requete4);

                    try {
                        Connexion co = new Connexion("projethopital", "root", "");
                        recup = co.remplirChampsRequete(requete1).toString(); ///Donne la valeur de la requete
                        recup = recup.substring(1, recup.length()-1); ///Enleve les crochets
                        nomEmployee.setText(recup);
                        recup = co.remplirChampsRequete(requete2).toString();
                        recup = recup.substring(1, recup.length()-1);
                        prenomEmployee.setText(recup);
                        recup = co.remplirChampsRequete(requete3).toString();
                        recup = recup.substring(1, recup.length()-1);
                        adresseEmployee.setText(recup);
                        recup = co.remplirChampsRequete(requete4).toString();
                        recup = recup.substring(1, recup.length()-2);
                        telEmployee.setText(recup);

                    } catch (SQLException | ClassNotFoundException ex) {
                        Logger.getLogger(FentreP.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    
                    modif.add(labelNomEmployee);
                    modif.add(nomEmployee);
                    modif.add(labelPrenomEmployee);
                    modif.add(prenomEmployee);
                    modif.add(labelAdresseEmployee);
                    modif.add(adresseEmployee);
                    modif.add(labelTelEmployee);
                    modif.add(telEmployee);
                    modif.add(valider);
            }
            ///Docteur
            if(choixModif.getSelectedItem() == "Modifier" && choixTab.getSelectedItem() == "Docteur" && e.getSource() == valider)
            {
                //Modification des valeurs par celles entré par l'utilisateur
                    requeteF = "specialite = " +  specialiteDocteur.getText() + " WHERE numero = " + numeroListeDocteur.getSelectedItem().toString();
                    String requete;
                    requete = "UPDATE " + choixTab.getSelectedItem().toString() + " SET " + requeteF ;
                    System.out.println(requete);
                    try {
                        Connexion co = new Connexion("projethopital", "root", "");
                        co.executeUpdate(requete);
                    } catch (SQLException | ClassNotFoundException ex) {
                            Logger.getLogger(FentreP.class.getName()).log(Level.SEVERE, null, ex);
                    }
                        requeteF = "";
            }
            else if(choixModif.getSelectedItem() == "Modifier" && choixTab.getSelectedItem() == "Docteur")
            {
                    ///Efface le contenue du panel
                    modif.removeAll();
                    modif.add(choixModif);
                    modif.add(choixTab);
                    modif.add(labelNumeroDocteur);
                    modif.add(numeroListeDocteur);
                 
                    ///Recuperation du codeListeService
                    String recup = new String(); ///string servant à modifier du texte sans l'altérer
                    recup = numeroListeDocteur.getSelectedItem().toString();
                    recup = recup.substring(0, recup.length()-1);
                    System.out.println("Recup: " + recup);
                                        
                    ///Etablissement des différentes requetes
                    String requete1 = "SELECT specialite FROM docteur WHERE numero = " + recup;
                    System.out.println("Requet1: " + requete1);

                    try {
                        Connexion co = new Connexion("projethopital", "root", "");
                        recup = co.remplirChampsRequete(requete1).toString(); ///Donne la valeur de la requete
                        recup = recup.substring(1, recup.length()-1); ///Enleve les crochets
                        System.out.println("Recuperation:" + recup);
                        specialiteDocteur.setText(recup);

                    } catch (SQLException | ClassNotFoundException ex) {
                        Logger.getLogger(FentreP.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    
                    modif.add(labelSpecialiteDocteur);
                    modif.add(specialiteDocteur);
                    modif.add(valider);
            }
            ///Infirmier
            if(choixModif.getSelectedItem() == "Modifier" && choixTab.getSelectedItem() == "Infirmier" && e.getSource() == valider)
            {
                //Modification des valeurs par celles entré par l'utilisateur
                    requeteF = "code_service = '" +  code_serviceInfirmier.getText() + "' , rotation = '" + rotationInfirmier.getSelectedItem().toString() +"' , salaire= " + salaireInfirmier.getText() + " WHERE numero = " + numeroListeInfirmier.getSelectedItem().toString();
                    String requete;
                    requete = "UPDATE " + choixTab.getSelectedItem().toString() + " SET " + requeteF ;
                    System.out.println(requete);
                    try {
                        Connexion co = new Connexion("projethopital", "root", "");
                        co.executeUpdate(requete);
                    } catch (SQLException | ClassNotFoundException ex) {
                            Logger.getLogger(FentreP.class.getName()).log(Level.SEVERE, null, ex);
                    }
                        requeteF = "";
            }
            else if(choixModif.getSelectedItem() == "Modifier" && choixTab.getSelectedItem() == "Infirmier")
            {
                    ///Efface le contenue du panel
                    modif.removeAll();
                    modif.add(choixModif);
                    modif.add(choixTab);
                    modif.add(labelNumeroInfirmier);
                    modif.add(numeroListeInfirmier);
                 
                    ///Recuperation du codeListeService
                    String recup = new String(); ///string servant à modifier du texte sans l'altérer
                    recup = numeroListeInfirmier.getSelectedItem().toString();
                    recup = recup.substring(0, recup.length()-1);
                    System.out.println("Recup: " + recup);
                                        
                    ///Etablissement des différentes requetes
                    String requete1 = "SELECT code_service FROM infirmier WHERE numero = " + recup;
                    String requete2 = "SELECT rotation FROM infirmier WHERE numero = " + recup;
                    String requete3 = "SELECT salaire FROM infirmier WHERE numero = " + recup;
                    System.out.println("Requet1: " + requete1);
                    System.out.println("Requet1: " + requete2);
                    System.out.println("Requet1: " + requete3);

                    try {
                        Connexion co = new Connexion("projethopital", "root", "");
                        recup = co.remplirChampsRequete(requete1).toString(); ///Donne la valeur de la requete
                        recup = recup.substring(1, recup.length()-1); ///Enleve les crochets
                        System.out.println("Recuperation:" + recup);
                        code_serviceInfirmier.setText(recup);
                        recup = co.remplirChampsRequete(requete2).toString(); ///Donne la valeur de la requete
                        recup = recup.substring(1, recup.length()-2); ///Enleve les crochets
                        System.out.println("Recuperation:" + recup);
                        if(recup == "JOUR")
                        {
                            rotationInfirmier.setSelectedIndex(0);
                            System.out.println("ok");
                        }
                        else
                        {
                            rotationInfirmier.setSelectedIndex(1);
                            System.out.println("okok");
                        }
                        recup = co.remplirChampsRequete(requete3).toString(); ///Donne la valeur de la requete
                        recup = recup.substring(1, recup.length()-2); ///Enleve les crochets
                        System.out.println("Recuperation:" + recup);
                        salaireInfirmier.setText(recup);
                        

                    } catch (SQLException | ClassNotFoundException ex) {
                        Logger.getLogger(FentreP.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    
                    modif.add(labelCode_serviceInfirmier);
                    modif.add(code_serviceInfirmier);
                    modif.add(labelRotationInfirmier);
                    modif.add(rotationInfirmier);
                    modif.add(labelSalaireInfirmier);
                    modif.add(salaireInfirmier);
                    modif.add(valider);
            }
            ///Malade
            if(choixModif.getSelectedItem() == "Modifier" && choixTab.getSelectedItem() == "Malade" && e.getSource() == valider)
            {
                //Modification des valeurs par celles entré par l'utilisateur
                    requeteF = "nom = '" +  nomMalade.getText() + "' , prenom = '" + prenomMalade.getText().toString() +"', tel= '" + telMalade.getText() + "' , adresse ='" + adresseMalade.getText() + "' , mutuelle ='" + mutuelleMalade.getText()  + "' WHERE numero = " + numeroListeMalade.getSelectedItem().toString();
                    String requete;
                    requete = "UPDATE " + choixTab.getSelectedItem().toString() + " SET " + requeteF ;
                    System.out.println(requete);
                    try {
                        Connexion co = new Connexion("projethopital", "root", "");
                        co.executeUpdate(requete);
                    } catch (SQLException | ClassNotFoundException ex) {
                            Logger.getLogger(FentreP.class.getName()).log(Level.SEVERE, null, ex);
                    }
                        requeteF = "";
            }
            else if(choixModif.getSelectedItem() == "Modifier" && choixTab.getSelectedItem() == "Malade")
            {
                    ///Efface le contenue du panel
                    modif.removeAll();
                    modif.add(choixModif);
                    modif.add(choixTab);
                    modif.add(labelNumeroMalade);
                    modif.add(numeroListeMalade);
                 
                    ///Recuperation du codeListeService
                    String recup = new String(); ///string servant à modifier du texte sans l'altérer
                    recup = numeroListeMalade.getSelectedItem().toString();
                    recup = recup.substring(0, recup.length()-1);
                    System.out.println("Recup: " + recup);
                                        
                    ///Etablissement des différentes requetes
                    String requete1 = "SELECT nom FROM malade WHERE numero = " + recup;
                    String requete2 = "SELECT prenom FROM malade WHERE numero = " + recup;
                    String requete3 = "SELECT tel FROM malade WHERE numero = " + recup;
                    String requete4 = "SELECT adresse FROM malade WHERE numero = " + recup;
                    String requete5 = "SELECT mutuelle FROM malade WHERE numero = " + recup;
                    System.out.println("Requet1: " + requete1);
                    System.out.println("Requet2: " + requete2);
                    System.out.println("Requet3: " + requete3);
                    System.out.println("Requet4: " + requete4);
                    System.out.println("Requet5: " + requete5);

                    try {
                        Connexion co = new Connexion("projethopital", "root", "");
                        recup = co.remplirChampsRequete(requete1).toString(); ///Donne la valeur de la requete
                        recup = recup.substring(1, recup.length()-1); ///Enleve les crochets
                        System.out.println("Recuperation:" + recup);
                        nomMalade.setText(recup);
                        recup = co.remplirChampsRequete(requete2).toString(); ///Donne la valeur de la requete
                        recup = recup.substring(1, recup.length()-1); ///Enleve les crochets
                        System.out.println("Recuperation:" + recup);
                        prenomMalade.setText(recup);
                        recup = co.remplirChampsRequete(requete3).toString(); ///Donne la valeur de la requete
                        recup = recup.substring(1, recup.length()-2); ///Enleve les crochets
                        System.out.println("Recuperation:" + recup);
                        telMalade.setText(recup);
                        recup = co.remplirChampsRequete(requete4).toString(); ///Donne la valeur de la requete
                        recup = recup.substring(1, recup.length()-1); ///Enleve les crochets
                        System.out.println("Recuperation:" + recup);
                        adresseMalade.setText(recup);
                        recup = co.remplirChampsRequete(requete5).toString(); ///Donne la valeur de la requete
                        recup = recup.substring(1, recup.length()-1); ///Enleve les crochets
                        System.out.println("Recuperation:" + recup);
                        mutuelleMalade.setText(recup);
                        

                    } catch (SQLException | ClassNotFoundException ex) {
                        Logger.getLogger(FentreP.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    
                    modif.add(labelNomMalade);
                    modif.add(nomMalade);
                    modif.add(labelPrenomMalade);
                    modif.add(prenomMalade);
                    modif.add(labelTelMalade);
                    modif.add(telMalade);
                    modif.add(labelAdresseMalade);
                    modif.add(adresseMalade);
                    modif.add(labelMutuelleMalade);
                    modif.add(mutuelleMalade);
                    modif.add(valider);
            }
            ///Hospitalisation
            if(choixModif.getSelectedItem() == "Modifier" && choixTab.getSelectedItem() == "Hospitalisation" && e.getSource() == valider)
            {
                //Modification des valeurs par celles entré par l'utilisateur
                    requeteF = "code_service = '" +  code_serviceHospitalisation.getText() + "' , no_chambre = " + no_chambreHospitalisation.getText() + ", lit= " + litHospitalisation.getText() + " WHERE no_malade = " + no_maladeListeHospitalisation.getSelectedItem().toString();
                    String requete;
                    requete = "UPDATE " + choixTab.getSelectedItem().toString() + " SET " + requeteF ;
                    System.out.println(requete);
                    try {
                        Connexion co = new Connexion("projethopital", "root", "");
                        co.executeUpdate(requete);
                    } catch (SQLException | ClassNotFoundException ex) {
                            Logger.getLogger(FentreP.class.getName()).log(Level.SEVERE, null, ex);
                    }
                        requeteF = "";
            }
            else if(choixModif.getSelectedItem() == "Modifier" && choixTab.getSelectedItem() == "Hospitalisation")
            {
                    ///Efface le contenue du panel
                    modif.removeAll();
                    modif.add(choixModif);
                    modif.add(choixTab);
                    modif.add(labelNo_maladeHospitalisation);
                    modif.add(no_maladeListeHospitalisation);
                 
                    ///Recuperation du codeListeService
                    String recup = new String(); ///string servant à modifier du texte sans l'altérer
                    recup = no_maladeListeHospitalisation.getSelectedItem().toString();
                    recup = recup.substring(0, recup.length()-1);
                    System.out.println("Recup: " + recup);
                                        
                    ///Etablissement des différentes requetes
                    String requete1 = "SELECT code_service FROM hospitalisation WHERE no_malade = " + recup;
                    String requete2 = "SELECT no_chambre FROM hospitalisation WHERE no_malade = " + recup;
                    String requete3 = "SELECT lit FROM hospitalisation WHERE no_malade = " + recup;
                    System.out.println("Requet1: " + requete1);
                    System.out.println("Requet2: " + requete2);
                    System.out.println("Requet3: " + requete3);

                    try {
                        Connexion co = new Connexion("projethopital", "root", "");
                        recup = co.remplirChampsRequete(requete1).toString(); ///Donne la valeur de la requete
                        recup = recup.substring(1, recup.length()-1); ///Enleve les crochets
                        System.out.println("Recuperation:" + recup);
                        code_serviceHospitalisation.setText(recup);
                        recup = co.remplirChampsRequete(requete2).toString(); ///Donne la valeur de la requete
                        recup = recup.substring(1, recup.length()-2); ///Enleve les crochets
                        System.out.println("Recuperation:" + recup);
                        no_chambreHospitalisation.setText(recup);
                        recup = co.remplirChampsRequete(requete3).toString(); ///Donne la valeur de la requete
                        recup = recup.substring(1, recup.length()-2); ///Enleve les crochets
                        System.out.println("Recuperation:" + recup);
                        litHospitalisation.setText(recup);

                    } catch (SQLException | ClassNotFoundException ex) {
                        Logger.getLogger(FentreP.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    
                    modif.add(labelCode_serviceHospitalisation);
                    modif.add(code_serviceHospitalisation);
                    modif.add(labelNo_chambreHospitalisation);
                    modif.add(no_chambreHospitalisation);
                    modif.add(labelLitHospitalisation);
                    modif.add(litHospitalisation);
                    modif.add(valider);
            }
            ///Pas de modification pour soignee
            
            ///Insertion**************************************************************************************************************************************************************
            ///Service
            if(choixModif.getSelectedItem() == "Inserer" && choixTab.getSelectedItem() == "Service") ///Choix de la table service
            {
                ///Efface le contenue du panel
                modif.removeAll();
                modif.add(choixModif);
                modif.add(choixTab);

                ///Ajout des lignes de saisies au panel modif
                modif.add(labelCodeService);
                modif.add(codeService);
                modif.add(labelNomService);
                modif.add(nomService);
                modif.add(labelBatimentService);
                modif.add(batimentService);
                modif.add(labelDirecteurService);
                modif.add(directeurService);
                modif.add(valider);
                
                if(e.getSource() == valider)
                {
                    requeteF = "'" + codeService.getText() + "'" + ", " + "'" + nomService.getText() + "'" + ", " + "'" + batimentService.getText()+ "'" + ", " + directeurService.getText() + ")";
                    String requete;
                    requete = "INSERT INTO " + choixTab.getSelectedItem() + " VALUES (" + requeteF ;
                    System.out.println(requete);
                    try {
                        Connexion co = new Connexion("projethopital", "root", "");
                        co.executeUpdate(requete);
                    } catch (SQLException | ClassNotFoundException ex) {
                        Logger.getLogger(FentreP.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    requeteF = "";
                }
            }
            
            else if(choixModif.getSelectedItem() == "Inserer" && choixTab.getSelectedItem() == "Chambre") ///Choix de la table chambre
            {
                ///Efface le contenue du panel
                modif.removeAll();
                modif.add(choixModif);
                modif.add(choixTab);

                ///Ajout des lignes de saisies au panel modif
                modif.add(labelCode_serviceChambre);
                modif.add(code_serviceChambre);
                modif.add(labelNo_chambreChambre);
                modif.add(no_chambreChambre);
                modif.add(labelSurveillantChambre);
                modif.add(surveillantChambre);
                modif.add(labelNb_litsChambre);
                modif.add(nb_litsChambre);
                modif.add(valider);
                
                if(e.getSource() == valider)
                {
                    requeteF = "'" + code_serviceChambre.getText() + "'" + ", " + no_chambreChambre.getText() + ", " + surveillantChambre.getText() + ", " + nb_litsChambre.getText() + ")";
                    String requete;
                    requete = "INSERT INTO " + choixTab.getSelectedItem() + " VALUES (" + requeteF ;
                    System.out.println(requete);
                    try {
                        Connexion co = new Connexion("projethopital", "root", "");
                        co.executeUpdate(requete);
                    } catch (SQLException | ClassNotFoundException ex) {
                        Logger.getLogger(FentreP.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    requeteF = "";
                }
            }
            
            else if(choixModif.getSelectedItem() == "Inserer" && choixTab.getSelectedItem() == "Employe") ///Choix de la table employee
            {
                ///Efface le contenue du panel
                modif.removeAll();
                modif.add(choixModif);
                modif.add(choixTab);

                ///Ajout des lignes de saisies au panel modif
                modif.add(labelNumeroEmployee);
                modif.add(numeroEmployee);
                modif.add(labelNomEmployee);
                modif.add(nomEmployee);
                modif.add(labelPrenomEmployee);
                modif.add(prenomEmployee);
                modif.add(labelAdresseEmployee);
                modif.add(adresseEmployee);
                modif.add(labelTelEmployee);
                modif.add(telEmployee);
                modif.add(valider);
                
                if(e.getSource() == valider)
                {
                    requeteF = numeroEmployee.getText() + ", " + "'" + nomEmployee.getText() + "'" + ", " + "'" + prenomEmployee.getText()+ "'" + ", " + "'" + adresseEmployee.getText() + "'" + ", " + telEmployee.getText() + ")";
                    String requete;
                    requete = "INSERT INTO " + choixTab.getSelectedItem() + " VALUES (" + requeteF ;
                    System.out.println(requete);
                    try {
                        Connexion co = new Connexion("projethopital", "root", "");
                        co.executeUpdate(requete);
                    } catch (SQLException | ClassNotFoundException ex) {
                        Logger.getLogger(FentreP.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    requeteF = "";
                    
                }
            }
            
            else if(choixModif.getSelectedItem() == "Inserer" && choixTab.getSelectedItem() == "Docteur") ///Choix de la table chambre
            {
                ///Efface le contenue du panel
                modif.removeAll();
                modif.add(choixModif);
                modif.add(choixTab);

                ///Ajout des lignes de saisies au panel modif
                modif.add(labelNumeroDocteur);
                modif.add(numeroDocteur);
                modif.add(labelSpecialiteDocteur);
                modif.add(specialiteDocteur);
                modif.add(valider);
                
                if(e.getSource() == valider)
                {
                    requeteF = numeroDocteur.getText() + ", "+ "'" + specialiteDocteur.getText()+ "'" + ")";
                    String requete;
                    requete = "INSERT INTO " + choixTab.getSelectedItem() + " VALUES (" + requeteF ;
                    System.out.println(requete);
                    try {
                        Connexion co = new Connexion("projethopital", "root", "");
                        co.executeUpdate(requete);
                    } catch (SQLException | ClassNotFoundException ex) {
                        Logger.getLogger(FentreP.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    requeteF = "";
                }
            
            }
            
            if(choixModif.getSelectedItem() == "Inserer" && choixTab.getSelectedItem() == "Infirmier") ///Choix de la table chambre
            {
               ///Efface le contenue du panel
                modif.removeAll();
                modif.add(choixModif);
                modif.add(choixTab);

                ///Ajout des lignes de saisies au panel modif
                modif.add(labelNumeroInfirmier);
                modif.add(numeroInfirmier);
                modif.add(labelCode_serviceInfirmier);
                modif.add(code_serviceInfirmier);
                modif.add(labelRotationInfirmier);
                modif.add(rotationInfirmier);
                modif.add(labelSalaireInfirmier);
                modif.add(salaireInfirmier);
                modif.add(valider);
                
                
                if(e.getSource() == valider)
                {
                requeteF = numeroInfirmier.getText() + ", " + "'" + code_serviceInfirmier.getText() + "'" + ", " + "'" + rotationInfirmier.getSelectedItem()+ "'" + ", " + salaireInfirmier.getText() + ")";
                    String requete;
                    requete = "INSERT INTO " + choixTab.getSelectedItem() + " VALUES (" + requeteF ;
                    System.out.println(requete);
                    try {
                        Connexion co = new Connexion("projethopital", "root", "");
                        co.executeUpdate(requete);
                    } catch (SQLException | ClassNotFoundException ex) {
                        Logger.getLogger(FentreP.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    requeteF = "";
                }
            }
            
            if(choixModif.getSelectedItem() == "Inserer" && choixTab.getSelectedItem() == "Malade") ///Choix de la table chambre
            {
               ///Efface le contenue du panel
                modif.removeAll();
                modif.add(choixModif);
                modif.add(choixTab);

                ///Ajout des lignes de saisies au panel modif
                modif.add(labelNumeroMalade);
                modif.add(numeroMalade);
                modif.add(labelNomMalade);
                modif.add(nomMalade);
                modif.add(labelPrenomMalade);
                modif.add(prenomMalade);
                modif.add(labelAdresseMalade);
                modif.add(adresseMalade);
                modif.add(labelTelMalade);
                modif.add(telMalade);
                modif.add(labelMutuelleMalade);
                modif.add(mutuelleMalade);
                modif.add(valider);
                
                
                if(e.getSource() == valider)
                {
                requeteF = numeroMalade.getText() + ", " + "'" + nomMalade.getText() + "'" + ", " + "'" + prenomMalade.getText()+ "'" + ", "+ "'" + adresseMalade.getText() + "'" + "," + telMalade.getText() + ", '" + mutuelleMalade.getText() + "'" + ")";
                    String requete;
                    requete = "INSERT INTO " + choixTab.getSelectedItem() + " VALUES (" + requeteF ;
                    System.out.println(requete);
                    try {
                        Connexion co = new Connexion("projethopital", "root", "");
                        co.executeUpdate(requete);
                    } catch (SQLException | ClassNotFoundException ex) {
                        Logger.getLogger(FentreP.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    requeteF = "";
                }
            }
            
            if(choixModif.getSelectedItem() == "Inserer" && choixTab.getSelectedItem() == "Hospitalisation") ///Choix de la table chambre
            {
               ///Efface le contenue du panel
                modif.removeAll();
                modif.add(choixModif);
                modif.add(choixTab);

                ///Ajout des lignes de saisies au panel modif
                modif.add(labelNo_maladeHospitalisation);
                modif.add(no_maladeHospitalisation);
                modif.add(labelCode_serviceHospitalisation);
                modif.add(code_serviceHospitalisation);
                modif.add(labelNo_chambreHospitalisation);
                modif.add(no_chambreHospitalisation);
                modif.add(labelLitHospitalisation);
                modif.add(litHospitalisation);
                modif.add(valider);
                
                if(e.getSource() == valider)
                {
                requeteF = no_maladeHospitalisation.getText() + ", " + "'" +code_serviceHospitalisation.getText() + "'" + ", "+ no_chambreHospitalisation.getText() + "," + litHospitalisation.getText() + ")";
                    String requete;
                    requete = "INSERT INTO " + choixTab.getSelectedItem() + " VALUES (" + requeteF ;
                    System.out.println(requete);
                    try {
                        Connexion co = new Connexion("projethopital", "root", "");
                        co.executeUpdate(requete);
                    } catch (SQLException | ClassNotFoundException ex) {
                        Logger.getLogger(FentreP.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    requeteF = "";
                }
            }
            
            if(choixModif.getSelectedItem() == "Inserer" && choixTab.getSelectedItem() == "Soignee") ///Choix de la table chambre
            {
               ///Efface le contenue du panel
                modif.removeAll();
                modif.add(choixModif);
                modif.add(choixTab);

                ///Ajout des lignes de saisies au panel modif
                modif.add(labelNo_docteurSoignee);
                modif.add(no_docteurSoignee);
                modif.add(labelNo_maladeSoignee);
                modif.add(no_maladeSoignee);
                modif.add(valider);
                
                
                if(e.getSource() == valider)
                {
                requeteF = no_docteurSoignee.getText()+ ", " + no_maladeSoignee.getText() + ")";
                    String requete;
                    requete = "INSERT INTO " + choixTab.getSelectedItem() + " VALUES (" + requeteF ;
                    System.out.println(requete);
                    try {
                        Connexion co = new Connexion("projethopital", "root", "");
                        co.executeUpdate(requete);
                    } catch (SQLException | ClassNotFoundException ex) {
                        Logger.getLogger(FentreP.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    requeteF = "";
                }
            }
            ///Infirmier
            
            ///Supression**************************************************************************************************************************************************************
            ///Service
            if(choixModif.getSelectedItem() == "Supprimer" && choixTab.getSelectedItem() == "Service" && e.getSource() == valider)
            {
                //Modification des valeurs par celles entré par l'utilisateur
                    requeteF = "code = '" +   codeListeService.getSelectedItem().toString().substring(0, codeListeService.getSelectedItem().toString().length()-1) + "'";
                    String requete;
                    requete = "DELETE FROM " + choixTab.getSelectedItem().toString() + " WHERE " + requeteF ;
                    System.out.println(requete);
                    try {
                        Connexion co = new Connexion("projethopital", "root", "");
                        co.executeUpdate(requete);
                    } catch (SQLException | ClassNotFoundException ex) {
                            Logger.getLogger(FentreP.class.getName()).log(Level.SEVERE, null, ex);
                    }
            }
            else if(choixModif.getSelectedItem() == "Supprimer" && choixTab.getSelectedItem() == "Service")
            {
                ///Efface le contenue du panel
                modif.removeAll();
                modif.add(choixModif);
                modif.add(choixTab);
                modif.add(labelCodeService);
                modif.add(codeListeService);
                modif.add(valider);
                
                
            }
            ///Chambre
            if(choixModif.getSelectedItem() == "Supprimer" && choixTab.getSelectedItem() == "Chambre" && e.getSource() == valider)
            {
                //Modification des valeurs par celles entré par l'utilisateur
                    requeteF = "no_chambre = " +   no_chambreListeChambre.getSelectedItem() + " AND code_service = '" + codeListeService.getSelectedItem().toString().substring(0, codeListeService.getSelectedItem().toString().length()-1)+ "'";
                    String requete;
                    requete = "DELETE FROM " + choixTab.getSelectedItem().toString() + " WHERE " + requeteF ;
                    System.out.println(requete);
                    try {
                        Connexion co = new Connexion("projethopital", "root", "");
                        co.executeUpdate(requete);
                    } catch (SQLException | ClassNotFoundException ex) {
                            Logger.getLogger(FentreP.class.getName()).log(Level.SEVERE, null, ex);
                    }
            }
            else if(choixModif.getSelectedItem() == "Supprimer" && choixTab.getSelectedItem() == "Chambre")
            {
                ///Efface le contenue du panel
                modif.removeAll();
                modif.add(choixModif);
                modif.add(choixTab);
                modif.add(labelNo_chambreChambre);
                modif.add(no_chambreListeChambre);
                modif.add(labelCodeService);
                modif.add(codeListeService);
                modif.add(valider);   
            }
            ///Employee
            if(choixModif.getSelectedItem() == "Supprimer" && choixTab.getSelectedItem() == "Employe" && e.getSource() == valider)
            {
                //Modification des valeurs par celles entré par l'utilisateur
                    requeteF = "numero = " +   numeroListeEmployee.getSelectedItem().toString().substring(0, numeroListeEmployee.getSelectedItem().toString().length()-1);
                    String requete;
                    String requete2;
                    String requete3;
                    requete = "DELETE FROM " + choixTab.getSelectedItem().toString() + " WHERE " + requeteF ;
                    requete2 = "DELETE FROM docteur WHERE no_malade = '" + numeroListeEmployee.getSelectedItem().toString().substring(0, numeroListeEmployee.getSelectedItem().toString().length()-1);
                    requete3 = "DELETE FROM infirmier WHERE no_malade ='" + numeroListeEmployee.getSelectedItem().toString().substring(0, numeroListeEmployee.getSelectedItem().toString().length()-1);
                    System.out.println(requete);
                    System.out.println(requete2);
                    System.out.println("ok");
                    System.out.println(requete3);
                    try {
                        Connexion co = new Connexion("projethopital", "root", "");
                        co.executeUpdate(requete);
                        co.executeUpdate(requete2);
                        co.executeUpdate(requete3);
                    } catch (SQLException | ClassNotFoundException ex) {
                            Logger.getLogger(FentreP.class.getName()).log(Level.SEVERE, null, ex);
                    }
            }
            else if(choixModif.getSelectedItem() == "Supprimer" && choixTab.getSelectedItem() == "Employe")
            {
                ///Efface le contenue du panel
                modif.removeAll();
                modif.add(choixModif);
                modif.add(choixTab);
                modif.add(labelNumeroEmployee);
                modif.add(numeroListeEmployee);
                modif.add(valider);   
            }
            ///Infirmier
            if(choixModif.getSelectedItem() == "Supprimer" && choixTab.getSelectedItem() == "Infirmier" && e.getSource() == valider)
            {
                //Modification des valeurs par celles entré par l'utilisateur
                    requeteF = "numero = " +   numeroListeInfirmier.getSelectedItem().toString().substring(0, numeroListeInfirmier.getSelectedItem().toString().length()-1);
                    String requete;
                    String requete2;
                    requete = "DELETE FROM " + choixTab.getSelectedItem().toString() + " WHERE " + requeteF ;
                    requete2 = "DELETE FROM employe WHERE " + requeteF ;
                    System.out.println(requete);
                    try {
                        Connexion co = new Connexion("projethopital", "root", "");
                        co.executeUpdate(requete);
                        co.executeUpdate(requete2);
                    } catch (SQLException | ClassNotFoundException ex) {
                            Logger.getLogger(FentreP.class.getName()).log(Level.SEVERE, null, ex);
                    }
            }
            else if(choixModif.getSelectedItem() == "Supprimer" && choixTab.getSelectedItem() == "Docteur")
            {
                ///Efface le contenue du panel
                modif.removeAll();
                modif.add(choixModif);
                modif.add(choixTab);
                modif.add(labelNumeroInfirmier);
                modif.add(numeroListeInfirmier);
                modif.add(valider);   
            }
            ///Docteur
            if(choixModif.getSelectedItem() == "Supprimer" && choixTab.getSelectedItem() == "Docteur" && e.getSource() == valider)
            {
                //Modification des valeurs par celles entré par l'utilisateur
                    requeteF = "numero = " +   numeroListeDocteur.getSelectedItem().toString().substring(0, numeroListeDocteur.getSelectedItem().toString().length()-1);
                    String requete;
                    String requete2;
                    requete = "DELETE FROM " + choixTab.getSelectedItem().toString() + " WHERE " + requeteF ;
                    requete2 = "DELETE FROM employe WHERE " + requeteF ;
                    System.out.println(requete);
                    try {
                        Connexion co = new Connexion("projethopital", "root", "");
                        co.executeUpdate(requete);
                    } catch (SQLException | ClassNotFoundException ex) {
                            Logger.getLogger(FentreP.class.getName()).log(Level.SEVERE, null, ex);
                    }
            }
            else if(choixModif.getSelectedItem() == "Supprimer" && choixTab.getSelectedItem() == "Docteur")
            {
                ///Efface le contenue du panel
                modif.removeAll();
                modif.add(choixModif);
                modif.add(choixTab);
                modif.add(labelNumeroDocteur);
                modif.add(numeroListeDocteur);
                modif.add(valider);   
            }
            ///Malade
            if(choixModif.getSelectedItem() == "Supprimer" && choixTab.getSelectedItem() == "Malade" && e.getSource() == valider)
            {
                //Modification des valeurs par celles entré par l'utilisateur
                    requeteF = "numero = " +   numeroListeMalade.getSelectedItem().toString().substring(0, numeroListeMalade.getSelectedItem().toString().length()-1);
                    String requete;
                    String requete2;
                    String requete3;
                    requete = "DELETE FROM " + choixTab.getSelectedItem().toString() + " WHERE " + requeteF;
                    requete2 = "DELETE FROM hospitalisation WHERE no_malade = " + numeroListeMalade.getSelectedItem().toString().substring(0, numeroListeMalade.getSelectedItem().toString().length()-1);
                    requete3 = "DELETE FROM soigne WHERE no_malade = " + numeroListeMalade.getSelectedItem().toString().substring(0, numeroListeMalade.getSelectedItem().toString().length()-1);
                    System.out.println(requete);
                    System.out.println(requete2);
                    System.out.println(requete3);
                    try {
                        Connexion co = new Connexion("projethopital", "root", "");
                        co.executeUpdate(requete);
                        co.executeUpdate(requete2);
                        co.executeUpdate(requete3);
                    } catch (SQLException | ClassNotFoundException ex) {
                            Logger.getLogger(FentreP.class.getName()).log(Level.SEVERE, null, ex);
                    }
            }
            else if(choixModif.getSelectedItem() == "Supprimer" && choixTab.getSelectedItem() == "Malade")
            {
                ///Efface le contenue du panel
                modif.removeAll();
                modif.add(choixModif);
                modif.add(choixTab);
                modif.add(labelNumeroMalade);
                modif.add(numeroListeMalade);
                modif.add(valider);
            }
            ///Hospitalisation
            if(choixModif.getSelectedItem() == "Supprimer" && choixTab.getSelectedItem() == "Hospitalisation" && e.getSource() == valider)
            {
                //Modification des valeurs par celles entré par l'utilisateur
                    requeteF = "no_malade = " +   numeroListeMalade.getSelectedItem().toString().substring(0, numeroListeMalade.getSelectedItem().toString().length()-1);
                    String requete;
                    String requete2;
                    requete = "DELETE FROM " + choixTab.getSelectedItem().toString() + " WHERE " + requeteF ;
                    requete2 = "DELETE FROM malade WHERE " + requeteF ;
                    System.out.println(requete);
                    try {
                        Connexion co = new Connexion("projethopital", "root", "");
                        co.executeUpdate(requete);
                    } catch (SQLException | ClassNotFoundException ex) {
                            Logger.getLogger(FentreP.class.getName()).log(Level.SEVERE, null, ex);
                    }
            }
            else if(choixModif.getSelectedItem() == "Supprimer" && choixTab.getSelectedItem() == "Hospitalisation")
            {
                ///Efface le contenue du panel
                modif.removeAll();
                modif.add(choixModif);
                modif.add(choixTab);
                modif.add(labelNumeroMalade);
                modif.add(numeroListeMalade);
                modif.add(valider);   
            }
            ///Soigne
            if(choixModif.getSelectedItem() == "Supprimer" && choixTab.getSelectedItem() == "Soigne" && e.getSource() == valider)
            {
                //Modification des valeurs par celles entré par l'utilisateur
                    requeteF = "no_malade = " +   numeroListeMalade.getSelectedItem().toString().substring(0, numeroListeMalade.getSelectedItem().toString().length()-1) + " AND no_docteur = " + numeroListeEmployee.getSelectedItem().toString().substring(0, numeroListeEmployee.getSelectedItem().toString().length()-1);
                    String requete;
                    String requete2;
                    requete = "DELETE FROM " + choixTab.getSelectedItem().toString() + " WHERE " + requeteF ;
                    requete2 = "DELETE FROM malade WHERE " + requeteF ;
                    System.out.println(requete);
                    try {
                        Connexion co = new Connexion("projethopital", "root", "");
                        co.executeUpdate(requete);
                    } catch (SQLException | ClassNotFoundException ex) {
                            Logger.getLogger(FentreP.class.getName()).log(Level.SEVERE, null, ex);
                    }

            }
            else if(choixModif.getSelectedItem() == "Supprimer" && choixTab.getSelectedItem() == "Soignee")
            {
                ///Efface le contenue du panel
                modif.removeAll();
                modif.add(choixModif);
                modif.add(choixTab);
                modif.add(labelNumeroMalade);
                modif.add(numeroListeMalade);
                modif.add(labelNumeroDocteur);
                modif.add(numeroListeEmployee);
                modif.add(valider);   
                
            }
        }
    }
}