/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele;

import controleur.FentreP;
import java.awt.Color;
import java.awt.GradientPaint;
import java.io.File;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import jdbcv2018.Connexion;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.ui.ApplicationFrame;
import org.jfree.ui.RefineryUtilities;

/**
* je compte la moyenne du salaire par service en sql, et je les mets dans un arraylist, avec une boucle
* for je parcours et met le tout dans un tableau normal en ajoutant par la meme occasion dans le dataset
* les moyennes et le service associe
 *
 */
public class BarChart2 extends ApplicationFrame {

    public BarChart2(final String title) throws Exception {

        super(title);
        final JFreeChart chart = createBarChart();
        
    }

    
    public JFreeChart createBarChart() throws Exception{
        
        ArrayList<String> salaire = new ArrayList<String>(); 

        
        try { ///Creation d'une connexion  
            Connexion co = new Connexion("projethopital", "root", "");
            salaire = co.remplirChampsRequete("SELECT code_service,AVG(salaire) FROM infirmier GROUP BY code_service");                       
            } catch (SQLException | ClassNotFoundException ex) {
                Logger.getLogger(FentreP.class.getName()).log(Level.SEVERE, null, ex);
            }
        
        final DefaultCategoryDataset dataset = new DefaultCategoryDataset();
        final String category1 = "service";
        
        String[] salaires;
        for(int i=0;i<salaire.size();i++)
        {   
            System.out.println(salaire.get(i));
            
            salaires=salaire.get(i).split(",");
            dataset.addValue(Float.parseFloat(salaires[1]), salaires[0], category1);
        }     
        // create the chart...
        final JFreeChart chart = ChartFactory.createBarChart(
            "Moyenne Salaires par service",         // chart title
            "",               // domain axis label
            "Salaires",                  // range axis label
            dataset,                  // data
            PlotOrientation.VERTICAL,
            true,                     // legende
            true,                     // tooltips
            false                     // URLs
        );


        // background couleur
        chart.setBackgroundPaint(new Color(0xBBBBDD));

        // get a reference to the plot for further customisation...
        final CategoryPlot plot = chart.getCategoryPlot();
        
        // set the range axis to display integers only...
        final NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis();
        rangeAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());

        // disable bar outlines...
        final BarRenderer renderer = (BarRenderer) plot.getRenderer();
        renderer.setDrawBarOutline(false);
        //renderer.setMaxBarWidth(0.10);
        
        // set up gradient paints for series...
        final GradientPaint gp0 = new GradientPaint(
            0.0f, 0.0f, Color.blue, 
            0.0f, 0.0f, Color.lightGray
        );
        final GradientPaint gp1 = new GradientPaint(
            0.0f, 0.0f, Color.green, 
            0.0f, 0.0f, Color.lightGray
        );
        
        renderer.setSeriesPaint(0, gp0);
        renderer.setSeriesPaint(1, gp1);
        
     
        int width = 480;   /* Width of the image */
      int height = 320;  /* Height of the image */ 
      File BarChart = new File( "BarChart2.jpeg" ); 
      ChartUtilities.saveChartAsJPEG( BarChart , chart , width , height );
      return chart;
        
    }
}
