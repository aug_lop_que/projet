/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele;

import controleur.FentreP;
import java.io.*;

import org.jfree.chart.ChartUtilities;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.data.general.DefaultPieDataset;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JPanel;
import jdbcv2018.Connexion;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PiePlot;
import org.jfree.chart.plot.PiePlot3D;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.ui.ApplicationFrame;
import org.jfree.ui.RefineryUtilities;


/**
 *
 * @author Raphael
 * code du graph camembert, il recoit le nb de lits dans un arraylist
 * il met le contenu de chaque case du arraylist dans des tring (j'aurais pu faire un tableau)
 * jutilise replaceAll qui remplave tout ce qui n'est pas des nombes par du vide, et apres 
 * j'utilise integer.parseint pour transformer en int
 * le .trim sert a enlever les crochets
 * je transforme en pourcentage et je l'affiche
 */
public class PieChart {
   
    public JFreeChart createPieChart() throws Exception {
        
        ArrayList<String> nb_lits = new ArrayList<String>(); 
        try { ///Creation d'une connexion  
            Connexion co = new Connexion("projethopital", "root", "");
            nb_lits = co.remplirChampsRequete("SELECT code_service,SUM(nb_lits) FROM chambre GROUP BY code_service"); ///Creation de la requete permettant de récupérer les différentes clées primaire de la classe service
            } catch (SQLException | ClassNotFoundException ex) {
                Logger.getLogger(FentreP.class.getName()).log(Level.SEVERE, null, ex);
            }           
      System.out.println(nb_lits);
      
      String service1=nb_lits.get(0);
      String service2=nb_lits.get(1);
      String service3=nb_lits.get(2);
           
      service1 = service1.replaceAll("[^0-9]+", " ");
      service2 = service2.replaceAll("[^0-9]+", " ");
      service3 = service3.replaceAll("[^0-9]+", " ");
      
      int i = Integer.parseInt(service1.trim());
      //double di=(double) i;
      int j = Integer.parseInt(service2.trim());
      //double dj=(double) j;
      int k = Integer.parseInt(service3.trim());
      //double dk=(double) k;
      
      int tot= i+j+k;
      i=i*100/tot;
      j=j*100/tot;
      k=k*100/tot;
      
      DefaultPieDataset dataset = new DefaultPieDataset( );
      dataset.setValue("Cardiologie", i );
      dataset.setValue("Chirurgie", j );
      dataset.setValue("Reanimation", k );

      JFreeChart chart = ChartFactory.createPieChart(
         "Repartition des Lits par Service en %",   // chart title
         dataset,          // data
         true,             // include legend
         true,
         false);
      
      int width = 480;   /* Width of the image */
      int height = 320;  /* Height of the image */ 
      File pieChart = new File( "PieChart.jpeg" ); 
      
      ChartUtilities.saveChartAsJPEG( pieChart , chart , width , height );
      
        return chart;
    }
}