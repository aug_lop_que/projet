/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele;

import controleur.FentreP;
import java.awt.Color;
import java.awt.GradientPaint;
import java.io.File;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import jdbcv2018.Connexion;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.ui.ApplicationFrame;
import org.jfree.ui.RefineryUtilities;

/**

* je compte les malades pour chaque mutuelle en sql, et je les mets dans un arraylist, avec une boucle
* for je parcours et met le tout dans un tableau normal en ajoutant par la meme occasion dans le dataset
* la mutuelle et le nombre associé
 */
public class BarChart extends ApplicationFrame {

    public BarChart(final String title) throws Exception {

        super(title);
        final JFreeChart chart = createBarChart();
        
    }

    
    public JFreeChart createBarChart() throws Exception{
        
        ArrayList<String> mutuelle = new ArrayList<String>(); 
        /*ArrayList<String> nb_ag2r = new ArrayList<String>(); 
        ArrayList<String> nb_ccvrp = new ArrayList<String>();
        ArrayList<String> nb_cnamts = new ArrayList<String>(); 
        ArrayList<String> nb_lmda = new ArrayList<String>(); 
        ArrayList<String> nb_maaf = new ArrayList<String>(); 
        ArrayList<String> nb_mas = new ArrayList<String>(); 
        ArrayList<String> nb_mgen = new ArrayList<String>(); 
        ArrayList<String> nb_mgsp = new ArrayList<String>(); 
        ArrayList<String> nb_mma = new ArrayList<String>(); 
        ArrayList<String> nb_mnam = new ArrayList<String>(); 
        ArrayList<String> nb_mnftc = new ArrayList<String>(); 
        ArrayList<String> nb_mnh = new ArrayList<String>();*/ 
        
        try { ///Creation d'une connexion  
            Connexion co = new Connexion("projethopital", "root", "");
            mutuelle = co.remplirChampsRequete("SELECT mutuelle,COUNT(numero) FROM malade GROUP BY mutuelle");
            /*nb_ag2r = co.remplirChampsRequete("SELECT DISTINCT mutuelle,COUNT(numero) FROM malade WHERE mutuelle='AG2R'"); ///Creation de la requete permettant de récupérer les différentes clées primaire de la classe service
            nb_ccvrp = co.remplirChampsRequete("SELECT DISTINCT mutuelle,COUNT(numero) FROM malade WHERE mutuelle='CCVRP'");
            nb_cnamts = co.remplirChampsRequete("SELECT DISTINCT mutuelle,COUNT(numero) FROM malade WHERE mutuelle='CNAMTS'");
            nb_lmda = co.remplirChampsRequete("SELECT DISTINCT mutuelle,COUNT(numero) FROM malade WHERE mutuelle='LMDA'");
            nb_maaf = co.remplirChampsRequete("SELECT DISTINCT mutuelle,COUNT(numero) FROM malade WHERE mutuelle='MAAF'");
            nb_mas = co.remplirChampsRequete("SELECT DISTINCT mutuelle,COUNT(numero) FROM malade WHERE mutuelle='MAS'");
            nb_mgen = co.remplirChampsRequete("SELECT DISTINCT mutuelle,COUNT(numero) FROM malade WHERE mutuelle='MGEN'");
            nb_mgsp = co.remplirChampsRequete("SELECT DISTINCT mutuelle,COUNT(numero) FROM malade WHERE mutuelle='MGSP'");
            nb_mma = co.remplirChampsRequete("SELECT DISTINCT mutuelle,COUNT(numero) FROM malade WHERE mutuelle='MMA'");
            nb_mnftc = co.remplirChampsRequete("SELECT DISTINCT mutuelle,COUNT(numero) FROM malade WHERE mutuelle='MNFTC'");
            nb_mnh = co.remplirChampsRequete("SELECT DISTINCT mutuelle,COUNT(numero) FROM malade WHERE mutuelle='MNH'");*/   
                        
            } catch (SQLException | ClassNotFoundException ex) {
                Logger.getLogger(FentreP.class.getName()).log(Level.SEVERE, null, ex);
            }
        
        final DefaultCategoryDataset dataset = new DefaultCategoryDataset();
        final String category1 = "mutuelle"
                + ""
                + ""
                + ""
                + "";
        
        String[] mutuelles;
        for(int i=0;i<mutuelle.size();i++)
        {   
            System.out.println(mutuelle.get(i));
            
            mutuelles=mutuelle.get(i).split(",");
            dataset.addValue(Float.parseFloat(mutuelles[1]), mutuelles[0], category1);
        }
        
        // row keys...
        /*final String series1 = "AG2R";
        final String series2 = "CCVRP";
        final String series3 = "CNAMTS";
        final String series4 = "LMDA";
        final String series5 = "MAAF";
        final String series6 = "MAS";
        final String series7 = "MGEN";
        final String series8 = "MGSP";
        final String series9 = "MMA";
        final String series10 = "MNAM";
        final String series11 = "MNFTC";
        final String series12 = "MNH";*/
              
       
        // create the chart...
        final JFreeChart chart = ChartFactory.createBarChart(
            "Nombres de malades par mutuelle",         // chart title
            "",               // domain axis label
            "Nombres de patients",                  // range axis label
            dataset,                  // data
            PlotOrientation.VERTICAL,
            true,                     // include legend
            true,                     // tooltips?
            false                     // URLs?
        );


        // set the background color for the chart...
        chart.setBackgroundPaint(new Color(0xBBBBDD));

        // get a reference to the plot for further customisation...
        final CategoryPlot plot = chart.getCategoryPlot();
        
        // set the range axis to display integers only...
        final NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis();
        rangeAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());

        // disable bar outlines...
        final BarRenderer renderer = (BarRenderer) plot.getRenderer();
        renderer.setDrawBarOutline(false);
        //renderer.setMaxBarWidth(0.10);
        
        // set up gradient paints for series...
        final GradientPaint gp0 = new GradientPaint(
            0.0f, 0.0f, Color.blue, 
            0.0f, 0.0f, Color.lightGray
        );
        final GradientPaint gp1 = new GradientPaint(
            0.0f, 0.0f, Color.green, 
            0.0f, 0.0f, Color.lightGray
        );
        
        renderer.setSeriesPaint(0, gp0);
        renderer.setSeriesPaint(1, gp1);
        
     
        int width = 480;   /* Width of the image */
      int height = 320;  /* Height of the image */ 
      File BarChart = new File( "BarChart.jpeg" ); 
      ChartUtilities.saveChartAsJPEG( BarChart , chart , width , height );
        return chart;
        
    }
}
